package JavaBasic.Homework4;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("New Pet class loaded");
    }

    {
        System.out.println("Object created");
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = Validation.petAge(age);
        this.trickLevel = Validation.petTrickLevel(trickLevel);
        this.habits = habits;
    }

    public Pet(String spices, String nickname) {
        this(spices, nickname, 0, 0, new String[]{});
    }

    public Pet() {
    }

    public String getSpices() {
        return species;
    }

    public void setSpices(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = Validation.petAge(age);
    }

    public int getTrickLevel() {

        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = Validation.petTrickLevel(trickLevel);
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s\n} ", getSpices(), getNickname(), getAge(), getTrickLevel(), Arrays.toString(getHabits()));
    }

    @Override
    public int hashCode() {
        int result = species.hashCode();
        result = 31 * result + nickname.hashCode();
        result = 31 * result + age;
        result = 31 * result + trickLevel;
        result = 31 * result + Arrays.deepHashCode(habits);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet pet = (Pet) o;

        if (age != pet.age) return false;
        if (trickLevel != pet.trickLevel) return false;
        if (!species.equals(pet.species)) return false;
        if (!nickname.equals(pet.nickname)) return false;
        return Arrays.deepEquals(habits, pet.habits);
    }

    public void eat() {
        System.out.printf("\n %s : Я ї'м!\n", getNickname());
    }

    public void respond() {
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив!\n", nickname);
    }

    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }
}