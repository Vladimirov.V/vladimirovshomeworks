package JavaBasic.Homework8;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private Map<DayOfWeek, String> schedule;

    static {
        System.out.println(Colors.GREEN_BOLD_BRIGHT+"New Human class loaded" + Colors.RESET);
    }

    {
        System.out.println(Colors.CYAN_BOLD_BRIGHT+"Object created" + Colors.RESET);
    }

    public Human(String name, String surname, int year, int iq, Family family, HashMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = Validation.humanYear(year);
        this.iq = Validation.humanIq(iq);
        this.family = family;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int iq, Family family) {
        this(name, surname, year, iq, family, null);
    }

    public Human(String name, String surname, int year) {
        this(name, surname, year, 0, null, null);
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = Validation.humanYear(year);
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = Validation.humanIq(iq);
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return String.format("Human{name='%s', surname='%s', year='%d', iq='%d', schedule=%s", name, surname, year, iq, schedule);
    }

@Override
public int hashCode() {
    return Objects.hash(name, surname, year, iq, schedule);
}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (year != human.year) return false;
        if (iq != human.iq) return false;
        if (!Objects.equals(name, human.name)) return false;
        if (!Objects.equals(surname, human.surname)) return false;
        return Objects.equals(schedule, human.schedule);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public boolean isTimeToFeed() {
        Random rnd = new Random();
        return rnd.nextBoolean();
    }
}