package JavaBasic.Homework8;

public class Cow extends Pet {

    public Cow() {
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d,can fly=%b, number of legs=%d, has fur=%b habits=%s} ", species, getNickname(), getAge(), getTrickLevel(), species.getCanFly(), species.getNumberOfLegs(), species.getHasFur(), getHabits());
    }

    @Override
    public void respond() {

    }
}