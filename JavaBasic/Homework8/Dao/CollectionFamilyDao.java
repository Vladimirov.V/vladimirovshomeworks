package JavaBasic.Homework8.Dao;

import JavaBasic.Homework8.Family;
import JavaBasic.Homework8.Human;
import JavaBasic.Homework8.Pet;

import java.time.Year;
import java.util.*;

public class CollectionFamilyDao implements FamilyDao {
    List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int idx) {
        if (families.size() > 0 && idx > -1 && idx < families.size()) {
            return families.get(idx);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int idx) {
        if (families.contains(families.get(idx))) {
            families.remove(idx);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (families.contains(family)) {
            families.remove(family);
            return true;
        }
        return false;
    }

    @Override
    public void saveFamily(Family family) {

        boolean familyExists = false;

        for (int i = 0; i < families.size(); i++) {
            Family currentFamily = families.get(i);

            if (currentFamily.getFather().equals(family.getFather()) && currentFamily.getMother().equals(family.getMother())) {
                families.set(i, family);
                familyExists = true;
                break;
            }
        }

        if (!familyExists) {
            families.add(family);
        }
    }

    @Override
    public String getMotherSurname(int familyIdx) {
        Family family = getFamilyByIndex(familyIdx);
        if (family != null) {
            Human mother = family.getFather();
            if (mother != null) {
                return mother.getSurname();
            }
        }
        return null;
    }

    @Override
    public String getFatherSurname(int familyIdx) {
        Family family = getFamilyByIndex(familyIdx);
        if (family != null) {
            Human father = family.getFather();
            if (father != null) {
                return father.getSurname();
            }
        }
        return null;
    }

    @Override
    public boolean setChild(int familyIdx, Human child) {
        Family family = getFamilyByIndex(familyIdx);
        if (family != null) {
            family.addChild(child);
            return true;
        }
        return false;
    }


    @Override
    public void deleteChild(Family family, Human child) {
        family.deleteChild(child);
    }

    @Override
    public ArrayList<Human> getAllChildren(int familyIdx) {
        Family family = getFamilyByIndex(familyIdx);
        if (family != null) {
            return family.getChildren();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public ArrayList<Human> getAllChildren() {
        ArrayList<Human> allChildren = new ArrayList<>();
        for (Family family : families) {
            for (Human child : family.getChildren()) {
                allChildren.add(child);
            }
        }
        return allChildren;
    }

    @Override
    public int count() {
        return families.size();
    }

    @Override
    public Set<Pet> getPets(int idx) {
        return getFamilyByIndex(idx).getPets();
    }

    @Override
    public boolean addPet(int familyIndex, Pet pet) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            family.addPet(pet);
            return true;
        }
        return false;
    }
}