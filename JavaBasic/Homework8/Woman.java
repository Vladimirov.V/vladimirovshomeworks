package JavaBasic.Homework8;

import java.util.HashMap;

public final class Woman extends Human {

    public Woman(String name, String surname, int year, int iq, Family family, HashMap<DayOfWeek, String> schedule) {
        super(name,surname,year,iq,family,schedule);
    }

    public void makeup() {
        System.out.println("Зараз підфарбуюся");
    }
}