package JavaBasic.Homework8;

import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

public abstract class Pet {
    protected Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    Set<String> habits;

    static {
        System.out.println(Colors.GREEN_BOLD_BRIGHT+"New Pet class loaded" + Colors.RESET);
    }

    {
        System.out.println(Colors.CYAN_BOLD_BRIGHT+"Object created" + Colors.RESET);
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = Validation.petAge(age);
        this.trickLevel = Validation.petTrickLevel(trickLevel);
        this.habits = habits;
    }

    public Pet(String nickname) {
        this(nickname, 0, 0, new HashSet<>());
    }

    public Pet() {
    }

    public Species getSpices() {
        return species;
    }

    public void setSpices(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = Validation.petAge(age);
    }

    public int getTrickLevel() {

        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = Validation.petTrickLevel(trickLevel);
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public abstract String toString();

    @Override
    public int hashCode() {
        return Objects.hash(species,nickname,age,trickLevel,habits);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet pet = (Pet) o;

        if (!Objects.equals(species, pet.species)) return false;
        if (!Objects.equals(nickname, pet.nickname)) return false;
        if (age != pet.age) return false;
        if (trickLevel != pet.trickLevel) return false;
        return Objects.equals(habits, pet.getHabits());
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public void eat() {
        System.out.printf("\n %s : Я ї'м!\n", getNickname());
    }

    public abstract void respond();

    public void greetPet() {
        System.out.println("Привіт, " + nickname);
    }

    private String trickOrNo(int trickLevel) {
        return trickLevel <= 50 ? "майже не хитрий" : "дуже хитрий";

    }

    public void describePet() {
        System.out.printf("У мене є %s, їй %d років, він %s\n", getSpices(), getAge(), trickOrNo(getTrickLevel()));
    }

    private void feedPetNow() {
        System.out.printf("Хм... нагодую я  %s\n", getNickname());
        eat();
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (isTimeToFeed) {
            feedPetNow();
            return true;
        }
        return ifIsNotTime();
    }

    private boolean ifIsNotTime() {
        Random rnd = new Random();
        int n = rnd.nextInt(101);
        if (getTrickLevel() > n) {

            feedPetNow();
            return true;
        } else {
            System.out.printf("Думаю, %s не голодний.\n", getNickname());
            return false;
        }
    }

}