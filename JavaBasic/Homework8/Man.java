package JavaBasic.Homework8;

import java.util.HashMap;

public final class Man extends Human {

    public Man(String name, String surname, int year, int iq, Family family, HashMap<DayOfWeek, String> schedule) {
  super(name,surname,year,iq,family,schedule);
    }

    public void repairCar() {
        System.out.println("Я пішов лагодити автівку");
    }
}