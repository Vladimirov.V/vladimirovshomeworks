package JavaBasic.Homework8;

import JavaBasic.Homework8.Controller.FamilyController;

import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {

        FamilyController familyController = new FamilyController();

        Human human = new Woman("Ann", "Javav", 1976, 89, null, new HashMap<>());
        Human human1 = new Man("Jack", "Javav", 1975, 90, null, new HashMap<>());
        Human human2 = new Woman("Alex", "Python", 1977, 80, null, new HashMap<>());
        Human human3 = new Man("Peter", "Python", 1978, 76, null, new HashMap<>());
        Human human4 = new Woman("Jane", "Script", 1965, 94, null, new HashMap<>());
        Human human5 = new Man("Jakob", "Script", 1966, 89, null, new HashMap<>());
        Human human6 = new Woman("Suzana", "C", 1961, 88, null, new HashMap<>());
        Human human7 = new Man("Ban", "C", 1963, 95, null, new HashMap<>());

        Human child = new Human("Dan", null, 2001);
        Human child2 = new Human("Max", null, 2004);
        Human child3 = new Human("Anton", null, 2003);

        Pet pet = new Dog("Sharik", 4, 55, new HashSet<>());
        Pet pet1 = new Fish("Fish", 4, 55, new HashSet<>());
        Pet pet2 = new DomesticCat("Murka", 4, 55, new HashSet<>());

        familyController.createNewFamily(human, human1);
        familyController.createNewFamily(human2, human3);
        familyController.createNewFamily(human4, human5);
        familyController.createNewFamily(human6, human7);

        familyController.bornChild(familyController.getFamilyById(0), "Angelika", "David");
        familyController.bornChild(familyController.getFamilyById(2), "Ann", "Alexandr");
        familyController.bornChild(familyController.getFamilyById(0), "Kate", "Mark");

        familyController.adoptChild(familyController.getFamilyById(1), child);
        familyController.adoptChild(familyController.getFamilyById(1), child2);
        familyController.adoptChild(familyController.getFamilyById(1), child3);

        familyController.addPet(1, pet);
        familyController.addPet(1, pet1);
        familyController.addPet(2, pet2);

        familyController.getFamiliesBiggerThan(3);
        familyController.getFamiliesLessThan(3);
        familyController.countFamiliesWithMemberNumber(3);

        //familyController.deleteAllChildrenOlderThen(18);

        familyController.deleteFamilyByIndex(0);

        System.out.println();
        System.out.println();
        System.out.println();
        familyController.displayAllFamilies();
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println(familyController.count());

        System.out.println(familyController.getFamilyById(2));

        System.out.println(familyController.getPets(1));
    }
}