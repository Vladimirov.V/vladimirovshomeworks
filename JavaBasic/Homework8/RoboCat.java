package JavaBasic.Homework8;

import java.util.Set;

public class RoboCat extends Pet {

    private final Species species = Species.ROBOCAT;

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Привіт, хазяїн. Я робот-кішка - %s. Я скучив!\n", getNickname());
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d,can fly=%b, number of legs=%d, has fur=%b habits=%s} ", species, getNickname(), getAge(), getTrickLevel(), species.getCanFly(), species.getNumberOfLegs(), species.getHasFur(), getHabits());
    }
}