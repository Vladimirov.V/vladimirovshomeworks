package JavaBasic.Homework5.Tests;

import JavaBasic.Homework5.Pet;
import JavaBasic.Homework5.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PetTests {
    @Test
    public void testToString() {
        Pet testPet = new Pet(Species.DOG, "Roy", 5, 78, new String[]{"eat", "sleep", "walking"});
        String result = testPet.toString();
        String trueResult = "DOG{nickname='Roy', age=5, trickLevel=78,can fly=false, number of legs=4, has fur=true habits=[eat, sleep, walking]} ";
        assertEquals(trueResult, result);
    }

    @Test
    public void testNotFullToString() {
        Pet testPet = new Pet(Species.DOG, "Roy");
        String result = testPet.toString();
        String trueResult = "DOG{nickname='Roy', age=0, trickLevel=0,can fly=false, number of legs=4, has fur=true habits=[]} ";
        assertEquals(trueResult, result);

    }

    @Test
    public void testNullPetToString() {
        Pet testPet = new Pet();
        String result = testPet.toString();
        String trueResult = "NULLSPECIES{nickname='null', age=0, trickLevel=0,can fly=false, number of legs=0, has fur=false habits=null} ";
        assertEquals(trueResult, result);
    }

    @Test
    public void testFullPetEquals() {
        Pet testPet1 = new Pet(Species.FISH, "Gold", 5, 55, new String[]{});
        Pet testPet2 = new Pet(Species.FISH, "Gold", 5, 55, new String[]{});

        assertTrue(testPet1.equals(testPet2));
    }

    @Test
    public void testFullPetNotEquals() {
        Pet testPet1 = new Pet(Species.FISH, "Gold", 5, 55, new String[]{});
        Pet testPet2 = new Pet(Species.FISH, "Silver", 4, 35, new String[]{});

        assertFalse(testPet1.equals(testPet2));
    }

    @Test
    public void testNotFullPetEquals() {
        Pet testPet1 = new Pet(Species.FISH, "Gold");
        Pet testPet2 = new Pet(Species.FISH, "Gold");

        assertTrue(testPet1.equals(testPet2));
    }

    @Test
    public void testNotFullPetNotEquals() {
        Pet testPet1 = new Pet(Species.FISH, "Gold");
        Pet testPet2 = new Pet(Species.FISH, "Silver");

        assertFalse(testPet1.equals(testPet2));
    }

    @Test
    public void testNullPetEquals() {
        Pet testPet1 = new Pet();
        Pet testPet2 = new Pet();

        assertTrue(testPet1.equals(testPet2));
    }

    @Test
    public void testNullPetNotEquals() {
        Pet testPet1 = new Pet();
        Pet testPet2 = new Pet(Species.FISH, "Gold");

        assertFalse(testPet1.equals(testPet2));
    }

    @Test
    public void testPetHashcode() {
        Pet testPet1 = new Pet(Species.FISH, "Gold");
        Pet testPet2 = new Pet(Species.FISH, "Gold");
        int result = testPet1.hashCode();
        int secondResult = testPet2.hashCode();
        assertEquals(result, secondResult);
    }
}