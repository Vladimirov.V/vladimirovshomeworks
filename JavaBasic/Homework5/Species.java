package JavaBasic.Homework5;

public enum Species {
    CAT(false,4,true),
    DOG(false,4,true),
    FISH(false,0,false),
    PIG(false,4,false),
    TIGER(false,4,true),
    NULLSPECIES();

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
    Species(){}

    private boolean canFly = false;
    private int numberOfLegs = 0;
    private boolean hasFur = false;

    public boolean getCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean getHasFur() {
        return hasFur;
    }
}