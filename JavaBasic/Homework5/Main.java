package JavaBasic.Homework5;

public class Main {
    public static void main(String[] args) {

        Pet roy = new Pet(Species.DOG, "Roy", 6, 77, new String[]{"walking"});

        System.out.println(roy);

        Human jack = new Human("Jack", "Java", 1986, 93, null, new String[][]{{DayOfWeek.MONDAY.name(), "Понеділок!"}, {DayOfWeek.TUESDAY.name(), "tues"}, {DayOfWeek.WEDNESDAY.name(), "wedn"}, {DayOfWeek.THURSDAY.name(), "thurs"}, {DayOfWeek.FRIDAY.name(), "Frid"}, {DayOfWeek.SATURDAY.name(), "Weekend"}, {DayOfWeek.SUNDAY.name(), "Weekende"}});

        System.out.println(jack);

        Human ann = new Human("Ann", "Java", 1987);

        Family family = new Family(ann, jack);

        Human child1 = new Human("Oleksandr", "Java", 2005, 94, family, new String[][]{{DayOfWeek.MONDAY.name(), "Понеділок!"}, {DayOfWeek.TUESDAY.name(), "tues"}, {DayOfWeek.WEDNESDAY.name(), "wedn"}, {DayOfWeek.THURSDAY.name(), "thurs"}, {DayOfWeek.FRIDAY.name(), "Frid"}, {DayOfWeek.SATURDAY.name(), "Weekend"}, {DayOfWeek.SUNDAY.name(), "Weekende"}});

        Human child2 = new Human("Oleksandr", "Java", 2005);

        System.out.println(child1);

        family.addChild(child1);
        family.addChild(child2);

        family.setPet(roy);

        System.out.println(family);

        //  System.out.println("Чи однакові child1 та child2?");
        //  System.out.println(child1.equals(child2));
        //  System.out.println(family);
        //  jack.feedPet(jack.isTimeToFeed());
        //  Human Oleg = new Human("Oleg", "Dobryden", 1968);
        //  System.out.println(Oleg);
        //  Human Angelina = new Human("Angelina", "Dobryden", 1971);
        //  System.out.println(Angelina);
        //  Human Peter = new Human("Peter", "Dobryden", 1998);
        //  System.out.println(Peter);
        //  Pet Jessi = new Pet(Species.FISH, "Jessi");
        //  System.out.println(Jessi);
        //  System.out.println("Чи однакові Peter та Oleg?");
        //  System.out.println(Peter.equals(Oleg));
        //  Jessi.eat();
        //  Jessi.foul();
        //  Family family1 = new Family(Angelina, Oleg);
        //  family1.addChild(Peter);
        //  family1.setPet(Jessi);
        //  Peter.greetPet();
        //  Peter.describePet();
        //  Jessi.respond();
        //  Peter.feedPet(Peter.isTimeToFeed());
        //  System.out.println(family1);

      //  for (int i = 0; i < 1000000; i++) {
      //      Human human = new Human();
      // }
    }
}