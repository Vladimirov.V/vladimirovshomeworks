package JavaBasic.Homework5;

import java.util.Arrays;

public class Family {

    private final Human mother;
    private final Human father;
    private Human[] children = new Human[]{};
    private Pet pet;

    static {
        System.out.println("New Family class loaded");
    }

    {
        System.out.println("Object created");
    }

    public Family(Human mother, Human father) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
    }

    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return this.father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Human getChild(Human child) {
        if (child == null || children.length == 0) return null;
        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) index = i;
        }
        return children[index];
    }


    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        String petStr = pet != null ? pet.toString() : "none";

        String childrenStr = "none";
        if (children.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            for (Human child : children) {
                if (child == null) continue;
                stringBuilder.append(String.format("\n\t %s", child));
            }
            stringBuilder.append("\n]");
            childrenStr = new String(stringBuilder);
        }
        return String.format("\n Family : \n mother=%s,\n father=%s,\n children=%s,\n pet=%s\n \n", mother, father, childrenStr, petStr);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] copy = Arrays.copyOf(children, children.length + 1);
        copy[copy.length - 1] = child;
        this.children = copy;
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index > children.length - 1) return false;
        Human[] copy = new Human[children.length - 1];
        int idx = 0;
        for (int i = 0; i < children.length; i++) {
            if (i == index) continue;
            copy[idx] = children[i];
            idx++;
        }
        children = copy;
        return true;
    }

    public boolean deleteChild(Human child) {
        if (child == null || children.length == 0) return false;
        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) index = i;
        }
        return deleteChild(index);
    }

    public int countFamily() {

        int count = children != null ? children.length : 0;
        if (father != null) count++;
        if (mother != null) count++;
        if (pet != null) count++;
        return count;
    }
}