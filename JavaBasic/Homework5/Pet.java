package JavaBasic.Homework5;

import java.util.Arrays;

public class Pet {
    private Species species = Species.NULLSPECIES;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("New Pet class loaded");
    }

    {
        System.out.println("Object created");
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = Validation.petAge(age);
        this.trickLevel = Validation.petTrickLevel(trickLevel);
        this.habits = habits;
    }

    public Pet(Species spices, String nickname) {
        this(spices, nickname, 0, 0, new String[]{});
    }

    public Pet() {
    }

    public Species getSpices() {
        return species;
    }

    public void setSpices(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = Validation.petAge(age);
    }

    public int getTrickLevel() {

        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = Validation.petTrickLevel(trickLevel);
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d,can fly=%b, number of legs=%d, has fur=%b habits=%s} ", getSpices(), getNickname(), getAge(), getTrickLevel(), species.getCanFly(),species.getNumberOfLegs(),species.getHasFur(), Arrays.toString(getHabits()));
    }

    @Override
    public int hashCode() {
        int result = species.hashCode();
        result = 31 * result + nickname.hashCode();
        result = 31 * result + age;
        result = 31 * result + trickLevel;
        result = 31 * result + Arrays.deepHashCode(habits);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (this.nickname == null && ((Pet) o).nickname == null) {

            Pet pet = (Pet) o;

            if (age != pet.age) return false;
            if (trickLevel != pet.trickLevel) return false;
            if (!species.equals(pet.species)) return false;
            return Arrays.deepEquals(habits, pet.habits);
        }

        Pet pet = (Pet) o;

        if (age != pet.age) return false;
        if (trickLevel != pet.trickLevel) return false;
        if (!species.equals(pet.species)) return false;
        if (!nickname.equals(pet.nickname)) return false;
        return Arrays.deepEquals(habits, pet.habits);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public void eat() {
        System.out.printf("\n %s : Я ї'м!\n", getNickname());
    }

    public void respond() {
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив!\n", nickname);
    }

    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }
}