package JavaBasic.Homework5;

import java.util.Arrays;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;

    static {
        System.out.println("New Human class loaded");
    }

    {
        System.out.println("Object created");
    }

    public Human(String name, String surname, int year, int iq, Family family, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = Validation.humanYear(year);
        this.iq = Validation.humanIq(iq);
        this.family = family;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int iq, Family family) {
        this(name, surname, year, iq, family, null);
    }

    public Human(String name, String surname, int year) {
        this(name, surname, year, 0, null);
    }

    public Human() {
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getSurname() {
        return surname;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    int getYear() {
        return year;
    }

    void setYear(int year) {
        this.year = Validation.humanYear(year);
    }

    int getIq() {
        return iq;
    }

    void setIq(int iq) {
        this.iq = Validation.humanIq(iq);
    }

    Family getFamily() {
        return family;
    }

    void setFamily(Family family) {
        this.family = family;
    }

    String[][] getSchedule() {
        return schedule;
    }

    void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return String.format("Human{name='%s', surname='%s', year='%d', iq='%d', schedule=%s", name, surname, year, iq, Arrays.deepToString(schedule));
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + year;
        result = 31 * result + iq;
        if (family == null ){
            result = 31 * result + year;
            result = 31 * result + Arrays.deepHashCode(schedule);
            return result;
        }
        result = 31 * result + family.hashCode();
        result = 31 * result + Arrays.deepHashCode(schedule);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (year != human.year) return false;
        if (iq != human.iq) return false;
        if (!name.equals(human.name) || name != null && human.name == null || name == null && human.name != null) return false;
        if (!surname.equals(human.surname)  || surname != null && human.surname == null || surname == null && human.surname != null) return false;
        if (!family.equals(human.family)  || family != null && human.family == null || family == null && human.family != null) return false;
        return Arrays.deepEquals(schedule, human.schedule);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public void greetPet() {
        if (family == null || family.getPet() == null) return;
        System.out.println("Привіт, " + family.getPet().getNickname());
    }

    private String trickOrNo(int trickLevel) {
        return trickLevel <= 50 ? "майже не хитрий" : "дуже хитрий";

    }

    public void describePet() {
        if (family == null || family.getPet() == null) return;
        Pet pet = family.getPet();
        System.out.printf("У мене є %s, їй %d років, він %s\n", pet.getSpices(), pet.getAge(), trickOrNo(pet.getTrickLevel()));
    }

    public boolean isTimeToFeed() {
        Random rnd = new Random();
        return rnd.nextBoolean();
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (family == null || family.getPet() == null) return false;
        if (isTimeToFeed) {
            feedPetNow();
            return true;
        }
        return ifIsNotTime();
    }

    private void feedPetNow() {
        System.out.printf("Хм... нагодую я  %s\n", family.getPet().getNickname());
        family.getPet().eat();
    }

    private boolean ifIsNotTime() {
        Random rnd = new Random();
        int n = rnd.nextInt(101);
        if (family.getPet().getTrickLevel() > n) {

            feedPetNow();
            return true;
        } else {
            System.out.printf("Думаю, %s не голодний.\n", family.getPet().getNickname());
            return false;
        }
    }
}