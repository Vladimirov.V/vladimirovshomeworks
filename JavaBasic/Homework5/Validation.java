package JavaBasic.Homework5;

import static JavaBasic.Homework5.Constants.*;

public class Validation {
    private static int isCorrectValue(int value, int minValue, int maxValue) {
        if (value <= minValue) return minValue;
        if (value >= maxValue) return maxValue;
        return value;
    }

    public static int petTrickLevel(int trickLevel) {
        return isCorrectValue(trickLevel, MIN_TRICK_LEVEL, MAX_TRICK_LEVEL);
    }

    public static int petAge(int age) {
        return isCorrectValue(age, MIN_PET_AGE, MAX_PET_AGE);

    }

    public static int humanIq(int iq) {
        return isCorrectValue(iq, MIN_HUMAN_IQ, MAX_HUMAN_IQ);
    }

    public static int humanYear(int year) {
        return isCorrectValue(year, MIN_HUMAN_YEAR, MAX_HUMAN_YEAR);
    }
}