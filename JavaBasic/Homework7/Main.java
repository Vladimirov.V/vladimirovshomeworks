package JavaBasic.Homework7;

import java.util.HashSet;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Dog roy = new Dog("Roy", 6, 77, new HashSet<>(List.of("walk","eat","sleep")));

        System.out.println(roy);

        DomesticCat cat = new DomesticCat("Murchik", 3, 99, new HashSet<>());

        System.out.println(cat);

        Fish fish = new Fish("Gold", 1, 1, new HashSet<>());

        System.out.println(fish);

        RoboCat roboCat = new RoboCat("Cat3000", 4, 0, new HashSet<>());

        System.out.println(roboCat);

        Pet cow = new Cow();
        System.out.println(cow);

        System.out.println();

        Man man = new Man();

        Woman woman = new Woman();

        Family family = new Family(man,woman);

        family.addPet(roy);
        family.addPet(cat);

        roy.feedPet(man.isTimeToFeed());

        System.out.println(man);

        System.out.println(family);

    }
}