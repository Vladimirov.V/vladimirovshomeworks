package JavaBasic.Homework7;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public abstract class Pet {
    protected Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    Set<String> habits;

    static {
        System.out.println("New Pet class loaded");
    }

    {
        System.out.println("Object created");
    }

    public Pet(String nickname, int age, int trickLevel, Set <String> habits) {
        this.nickname = nickname;
        this.age = Validation.petAge(age);
        this.trickLevel = Validation.petTrickLevel(trickLevel);
        this.habits = habits;
    }

    public Pet(String nickname) {
        this(nickname, 0, 0, new HashSet<>());
    }

    public Pet() {
    }

    public Species getSpices() {
        return species;
    }

    public void setSpices(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = Validation.petAge(age);
    }

    public int getTrickLevel() {

        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = Validation.petTrickLevel(trickLevel);
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public abstract String toString();

    @Override
    public int hashCode() {
        int result = species.hashCode();
        result = 31 * result + nickname.hashCode();
        result = 31 * result + age;
        result = 31 * result + trickLevel;
        result = 31 * result + habits.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (this.nickname == null && ((Pet) o).getNickname() == null ) {

            if (this.habits == null && ((Pet)o).getHabits() == null){

                Pet pet = (Pet) o;

                if (age != pet.getAge()) return false;
                if (trickLevel != pet.getTrickLevel()) return false;
                return  (species.equals(pet.getSpices()));
            }

            Pet pet = (Pet) o;

            if (age != pet.getAge()) return false;
            if (trickLevel != pet.getTrickLevel()) return false;
            if (!species.equals(pet.getSpices())) return false;
            return habits.equals(pet.getHabits());
        }

        Pet pet = (Pet) o;

        if (age != pet.age) return false;
        if (trickLevel != pet.trickLevel) return false;
        if (!species.equals(pet.species)) return false;
        if (!nickname.equals(pet.nickname)) return false;
        return habits.equals(pet.getHabits());
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public void eat() {
        System.out.printf("\n %s : Я ї'м!\n", getNickname());
    }

    public abstract void respond();

    public void greetPet() {
        System.out.println("Привіт, " + nickname);
    }

    private String trickOrNo(int trickLevel) {
        return trickLevel <= 50 ? "майже не хитрий" : "дуже хитрий";

    }

    public void describePet() {
        System.out.printf("У мене є %s, їй %d років, він %s\n", getSpices(), getAge(), trickOrNo(getTrickLevel()));
    }

    private void feedPetNow() {
        System.out.printf("Хм... нагодую я  %s\n", getNickname());
        eat();
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (isTimeToFeed) {
            feedPetNow();
            return true;
        }
        return ifIsNotTime();
    }

    private boolean ifIsNotTime() {
        Random rnd = new Random();
        int n = rnd.nextInt(101);
        if (getTrickLevel() > n) {

            feedPetNow();
            return true;
        } else {
            System.out.printf("Думаю, %s не голодний.\n", getNickname());
            return false;
        }
    }

}