package JavaBasic.Homework7;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    Map<DayOfWeek, String> schedule;

    static {
        System.out.println("New Human class loaded");
    }

    {
        System.out.println("Object created");
    }

    public Human(String name, String surname, int year, int iq, Family family, HashMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = Validation.humanYear(year);
        this.iq = Validation.humanIq(iq);
        this.family = family;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int iq, Family family) {
        this(name, surname, year, iq, family, null);
    }

    public Human(String name, String surname, int year) {
        this(name, surname, year, 0, null, null);
    }

    public Human() {
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getSurname() {
        return surname;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    int getYear() {
        return year;
    }

    void setYear(int year) {
        this.year = Validation.humanYear(year);
    }

    int getIq() {
        return iq;
    }

    void setIq(int iq) {
        this.iq = Validation.humanIq(iq);
    }

    Family getFamily() {
        return family;
    }

    void setFamily(Family family) {
        this.family = family;
    }

    Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return String.format("Human{name='%s', surname='%s', year='%d', iq='%d', schedule=%s", name, surname, year, iq, schedule);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + year;
        result = 31 * result + iq;
        result = 31 * result + family.hashCode();
        result = 31 * result + schedule.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (year != human.year) return false;
        if (iq != human.iq) return false;
        if (!name.equals(human.name)) return false;
        if (!surname.equals(human.surname)) return false;
        if (!family.equals(human.family)) return false;
        return schedule.equals(human.schedule);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public boolean isTimeToFeed() {
        Random rnd = new Random();
        return rnd.nextBoolean();
    }
}