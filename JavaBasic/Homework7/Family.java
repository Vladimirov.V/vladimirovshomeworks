package JavaBasic.Homework7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Family {

    private final Human mother;
    private final Human father;
    ArrayList<Human> children = new ArrayList<>();
    Set<Pet> pets = new HashSet<>();

    static {
        System.out.println("New Family class loaded");
    }

    {
        System.out.println("Object created");
    }

    public Family(Human mother, Human father) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
    }

    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return this.father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public Human getChild(Human child) {
        if (child == null || children.size() == 0) return null;
        int index = -1;
        for (int i = 0; i < children.size(); i++) {
            if (children.get(i).equals(child)) index = i;
        }
        return children.get(index);
    }


    public void setChildren(ArrayList<Human> children) {
        this.children.addAll(children);
    }

    public Set<Pet> getPets() {
        return this.pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        String petStr = pets != null ? pets.toString() : "none";

        String childrenStr = "none";
        if (children.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            for (Human child : children) {
                if (child == null) continue;
                stringBuilder.append(String.format("\n\t %s", child));
            }
            stringBuilder.append("\n]");
            childrenStr = new String(stringBuilder);
        }
        return String.format("\n Family : \n mother=%s,\n father=%s,\n children=%s,\n pet=%s\n \n", mother, father, childrenStr, petStr);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public void addChild(Human child) {
        if (children == null) {
            ArrayList<Human> childrenS = new ArrayList<>();
            childrenS.add(child);
            this.children = childrenS;
            return;
        }
        child.setFamily(this);
        children.add(child);
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index > children.size() - 1) return false;
        children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        if (child == null || children.size() == 0) return false;
        children.remove(child);
        return true;
    }

    public int countFamily() {

        int count = children != null ? children.size() : 0;
        if (father != null) count++;
        if (mother != null) count++;
        if (pets != null) count++;
        return count;
    }
}