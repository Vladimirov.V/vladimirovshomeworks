package JavaBasic.Homework9.Service;

import JavaBasic.Homework9.Colors;
import JavaBasic.Homework9.Dao.CollectionFamilyDao;
import JavaBasic.Homework9.Dao.FamilyDao;
import JavaBasic.Homework9.Family;
import JavaBasic.Homework9.Human;
import JavaBasic.Homework9.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyService {
    FamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        System.out.println(Colors.GREEN + familyDao.getAllFamilies() + Colors.RESET);
    }

    public ArrayList<Family> getFamiliesBiggerThan(int number) {
        ArrayList<Family> familiesBiggerThan = new ArrayList<>();

        for (Family family : getAllFamilies()) {
            if (family != null && family.countFamily() > number) {
                familiesBiggerThan.add(family);
            }
        }
        return familiesBiggerThan;
    }

    public ArrayList<Family> getFamiliesLessThan(int number) {
        ArrayList<Family> familiesLessThan = new ArrayList<>();

        for (Family family : getAllFamilies()) {
            if (family != null && family.countFamily() < number) {
                familiesLessThan.add(family);
            }
        }
        return familiesLessThan;
    }

    public ArrayList<Family> countFamiliesWithMemberNumber(int number) {
        ArrayList<Family> familiesWithMemberNumber = new ArrayList<>();

        for (Family family : getAllFamilies()) {
            if (family.countFamily() == number) {
                familiesWithMemberNumber.add(family);
            }
        }
        return familiesWithMemberNumber;
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
    }

    public void deleteFamilyByIndex(int idx) {
        familyDao.deleteFamily(idx);
    }

    public Family bornChild(Family family, String womenName, String manName) {
        boolean isBoy = Math.random() > 0.5;

        if (isBoy) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String currentDateFormatted = LocalDate.now().format(formatter);

            Human child = new Human(manName, familyDao.getFatherSurname(familyDao.getAllFamilies().indexOf(family)), currentDateFormatted);
            adoptChild(family, child);
            familyDao.saveFamily(family);
            return family;
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String currentDateFormatted = LocalDate.now().format(formatter);

            Human child = new Human(womenName, familyDao.getMotherSurname(familyDao.getAllFamilies().indexOf(family)), currentDateFormatted);
            adoptChild(family, child);
            familyDao.saveFamily(family);
            return family;
        }
    }

    public Family adoptChild(Family family, Human child) {
        familyDao.setChild(familyDao.getAllFamilies().indexOf(family), child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {

        for (Family family : familyDao.getAllFamilies()) {
            List<Human> childrenToRemove = new ArrayList<>();

            for (Human child : family.getChildren()) {
                long birthTimestamp = child.getBirthDate();
                LocalDate birthDate = Instant.ofEpochMilli(birthTimestamp)
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();

                LocalDate ageLimitDate = birthDate.plusYears(age);

                if (LocalDate.now().isAfter(ageLimitDate)) {
                    childrenToRemove.add(child);
                }
            }

            for (Human childToRemove : childrenToRemove) {
                family.deleteChild(childToRemove);
            }

            familyDao.saveFamily(family);
        }
    }

    public ArrayList<Human> getAllChildren() {
        return familyDao.getAllChildren();
    }

    public int count() {
        return familyDao.count();
    }

    public Family getFamilyById(int idx) {
        return familyDao.getFamilyByIndex(idx);
    }

    public Set<Pet> getPets(int idx) {
        return familyDao.getPets(idx);
    }

    public void addPet(int idx, Pet pet) {
        familyDao.addPet(idx, pet);
    }

}