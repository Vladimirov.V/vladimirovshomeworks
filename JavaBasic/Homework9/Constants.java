package JavaBasic.Homework9;

import java.time.Year;

public final class Constants {
    public static final int MIN_TRICK_LEVEL = 0;
    public static final int MAX_TRICK_LEVEL = 100;
    public static final int MIN_PET_AGE = 0;
    public static final int MAX_PET_AGE = Integer.MAX_VALUE - 1;
    public static final int MIN_HUMAN_IQ = 0;
    public static final int MAX_HUMAN_IQ = 100;
    public static final long MAX_HUMAN_YEAR = Year.now().getValue();
    public static final long MIN_HUMAN_YEAR = MAX_HUMAN_YEAR - 150;
}