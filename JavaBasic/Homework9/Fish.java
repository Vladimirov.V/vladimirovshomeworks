package JavaBasic.Homework9;

import java.util.Set;

public class Fish extends Pet {

    private final Species species = Species.FISH;

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Привіт, хазяїн. Я рибка - %s. Буль-буль!\n", getNickname());
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d,can fly=%b, number of legs=%d, has fur=%b habits=%s} ", species, getNickname(), getAge(), getTrickLevel(), species.getCanFly(), species.getNumberOfLegs(), species.getHasFur(), getHabits());
    }
}