package JavaBasic.Homework9.Tests;

import JavaBasic.Homework9.Dog;
import JavaBasic.Homework9.Family;
import JavaBasic.Homework9.Human;
import JavaBasic.Homework9.Pet;
import JavaBasic.Homework9.Service.FamilyService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class ServiceTests {

    private FamilyService familyService;

    @Before
    public void setUp() {

        familyService = new FamilyService();

        Human mother1 = new Human("Test", "Mother1", "01/01/1967", 97, null, new HashMap<>());
        Human father1 = new Human("Test", "Father1", "01/01/1970", 65, null, new HashMap<>());
        Human mother2 = new Human("Test", "Mother2", "01/01/1979", 82, null, new HashMap<>());
        Human father2 = new Human("Test", "Father2", "01/01/1980", 61, null, new HashMap<>());
        Human mother3 = new Human("Test", "Mother3", "01/01/1960", 67, null, new HashMap<>());
        Human father3 = new Human("Test", "Father3", "01/01/1964", 77, null, new HashMap<>());

        Human testChild1 = new Human("Test", "Child1", "01/01/2000", 54, null, new HashMap<>());
        Human testChild2 = new Human("Test", "Child2", "01/01/2002", 65, null, new HashMap<>());
        Human testChild3 = new Human("Test", "Child3", "01/01/2007", 77, null, new HashMap<>());

        Pet testPet1 = new Dog("test1", 4, 34, new HashSet<>());
        Pet testPet2 = new Dog("test2", 5, 44, new HashSet<>());
        Pet testPet3 = new Dog("test3", 6, 74, new HashSet<>());

        familyService.createNewFamily(mother1, father1);
        familyService.createNewFamily(mother2, father2);
        familyService.createNewFamily(mother3, father3);

        familyService.addPet(2, testPet1);
        familyService.addPet(2, testPet2);
        familyService.addPet(1, testPet3);

        familyService.adoptChild(familyService.getFamilyById(2), testChild1);
        familyService.adoptChild(familyService.getFamilyById(1), testChild2);
        familyService.adoptChild(familyService.getFamilyById(0), testChild3);

    }

    @Test
    public void testGetAllFamilies() {

        ArrayList<Family> trueFamilies = new ArrayList<>();
        trueFamilies.add(familyService.getFamilyById(0));
        trueFamilies.add(familyService.getFamilyById(1));
        trueFamilies.add(familyService.getFamilyById(2));

        assertEquals(trueFamilies, familyService.getAllFamilies());
    }

    @Test
    public void testGetFamiliesBiggerThan() {

        ArrayList<Family> trueFamilies = new ArrayList<>();
        trueFamilies.add(familyService.getFamilyById(1));
        trueFamilies.add(familyService.getFamilyById(2));

        assertEquals(trueFamilies, familyService.getFamiliesBiggerThan(3));
    }

    @Test
    public void testGetFamiliesLessThan() {

        ArrayList<Family> trueFamilies = new ArrayList<>();
        trueFamilies.add(familyService.getFamilyById(0));

        assertEquals(trueFamilies, familyService.getFamiliesLessThan(4));

    }

    @Test
    public void testCountFamiliesWithMemberNumber() {

        ArrayList<Family> trueFamilies = new ArrayList<>();
        trueFamilies.add(familyService.getFamilyById(0));

        assertEquals(trueFamilies, familyService.countFamiliesWithMemberNumber(3));
    }

    @Test
    public void testCreateNewFamily() {
        int oldCount = familyService.count();

        Human testhuman = new Human();
        Human testHuman1 = new Human();

        familyService.createNewFamily(testhuman, testHuman1);
        assertEquals(oldCount + 1, familyService.count());
        familyService.deleteFamilyByIndex(3);
    }

    @Test
    public void testDeleteFamilyByIndex() {
        int oldCount = familyService.getAllFamilies().size();

        familyService.deleteFamilyByIndex(2);

        int newCount = familyService.getAllFamilies().size();

        assertEquals(oldCount - 1, newCount);
    }

    @Test
    public void testBornChild() {

        int oldCount = familyService.getFamilyById(0).countFamily();

        familyService.bornChild(familyService.getFamilyById(0), "Ann", "Ben");
        assertEquals(oldCount + 1, familyService.getFamilyById(0).countFamily());
    }

    @Test
    public void testAdoptChild() {

        int oldCount = familyService.getFamilyById(0).countFamily();

        Human testChild = new Human("AdoptedChild", "Surname1", "01/01/2010");

        familyService.adoptChild(familyService.getFamilyById(0), testChild);

        assertEquals(oldCount + 1, familyService.getFamilyById(0).countFamily());
    }

    @Test
    public void testDeleteAllChildrenOlderThen() {

        int oldCount = familyService.getAllChildren().size();

        familyService.deleteAllChildrenOlderThen(18);

        assertEquals(oldCount - 2, familyService.getAllChildren().size());
    }
}