package JavaBasic.Homework9.Tests;

import JavaBasic.Homework9.DayOfWeek;
import JavaBasic.Homework9.Family;
import JavaBasic.Homework9.Human;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class HumanTests {
    @Test
    public void testToString() {
        Family familyMock = Mockito.mock(Family.class);
        Human testHuman = new Human("Vasily", "Java", "01/01/1988", 97, familyMock, new HashMap<DayOfWeek, String>() {{
            put(DayOfWeek.MONDAY, "go to the cinema");
        }});
        String result = testHuman.toString();
        String trueResult = "Human{name='Vasily', surname='Java', birth date: '01/01/1988', iq='97', schedule={MONDAY=go to the cinema}";
        assertEquals(trueResult, result);
    }

    @Test
    public void testNotFullToString() {
        Family familyMock = Mockito.mock(Family.class);
        Human testHuman = new Human("Vasily", "Java", "01/01/1988", 97, familyMock);
        String result = testHuman.toString();
        String trueResult = "Human{name='Vasily', surname='Java', birth date: '01/01/1988', iq='97', schedule=null";
        assertEquals(trueResult, result);
    }

    @Test
    public void testVeryNotFullToString() {
        Human testHuman = new Human("Vasily", "Java", "01/01/1988");
        String result = testHuman.toString();
        String trueResult = "Human{name='Vasily', surname='Java', birth date: '01/01/1988', iq='0', schedule=null";
        assertEquals(trueResult, result);
    }

    @Test
    public void testNullToString() {
        Human testHuman = new Human();
        String result = testHuman.toString();
        String trueResult = "Human{name='null', surname='null', birth date: '01/01/1970', iq='0', schedule=null";
        assertEquals(trueResult, result);
    }

    @Test
    public void testFullHumanEquals() {
        Family mockFamily = new Mockito().mock(Family.class);

        Human testHuman1 = new Human("Ivan", "Ivanovich", "01/01/1986", 75, mockFamily, new HashMap<DayOfWeek, String>());
        Human testHuman2 = new Human("Ivan", "Ivanovich", "01/01/1986", 75, mockFamily, new HashMap<DayOfWeek, String>());
        assertTrue(testHuman1.equals(testHuman2));
    }

    @Test
    public void testFullHumanNotEquals() {
        Family mockFamily = new Mockito().mock(Family.class);

        Human testHuman1 = new Human("Ivan", "Ivanovich", "01/01/1986", 75, mockFamily, new HashMap<DayOfWeek, String>());
        Human testHuman2 = new Human("Ivana", "Ivanovish", "01/01/1988", 77, mockFamily, new HashMap<DayOfWeek, String>());

        assertFalse(testHuman1.equals(testHuman2));
    }

    @Test
    public void testNotFullHumanEquals() {
        Human testHuman1 = new Human("Ivan", "Ivanovich", "01/01/1986");
        Human testHuman2 = new Human("Ivan", "Ivanovich", "01/01/1986");
        assertTrue(testHuman1.equals(testHuman2));
        System.err.println("TODO : problem with null");
    }

    @Test
    public void testNotFullHumanNotEquals() {
        Human testHuman1 = new Human("Ivan", "Ivanovich", "01/01/1986");
        Human testHuman2 = new Human("Ivana", "Ivanovish", "01/01/1984");

        assertFalse(testHuman1.equals(testHuman2));
    }

    @Test
    public void testNullHumanEquals() {
        Human testHuman1 = new Human();
        Human testHuman2 = new Human();
        assertTrue(testHuman1.equals(testHuman2));
        System.err.println("TODO : problem with null");
    }

    @Test
    public void testNullHumanNotEquals() {
        Human testHuman1 = new Human();
        Human testHuman2 = new Human("Ivan", "Ivanovich", "01/01/1986");

        assertFalse(testHuman1.equals(testHuman2));
    }

    @Test
    public void testHumanHashcode() {
        Family mockFamily = new Mockito().mock(Family.class);

        Human testHuman1 = new Human("Ivan", "Ivanovich", "01/01/1986", 55, mockFamily, new HashMap<DayOfWeek, String>());
        Human testHuman2 = new Human("Ivan", "Ivanovich", "01/01/1986", 55, mockFamily, new HashMap<DayOfWeek, String>());
        int result = testHuman1.hashCode();
        int secondResult = testHuman2.hashCode();
        assertEquals(result, secondResult);
    }
}