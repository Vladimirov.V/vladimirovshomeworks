package JavaBasic.Homework12.Utils;

import JavaBasic.Homework12.Models.Family;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileWorker {

    private static final String FILENAME = "family_data.dat";

    public static void saveDataToFile(List<Family> families) {
        createFileIfNotExists();
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILENAME))) {
            oos.writeObject(families);
        } catch (IOException e) {
            System.err.println("Error saving data: " + e.getMessage());
        }
    }

    public static List<Family> loadDataFromFile() {
        createFileIfNotExists();
        List<Family> familyData = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILENAME))) {
            familyData.addAll((List<Family>) ois.readObject());
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error loading data: " + e.getMessage());
        }
        return !familyData.isEmpty() ? familyData : new ArrayList<>();
    }

    private static void createFileIfNotExists() {
        File file = new File(FILENAME);
        if (!file.exists()) {
            try {
                if (file.createNewFile()) {
                    System.out.println("File created: " + FILENAME);
                }
            } catch (IOException e) {
                System.err.println("Error creating file: " + e.getMessage());
            }
        }
    }

}
