package JavaBasic.Homework12.Utils;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException(String message) {
        super(message);
    }
}
