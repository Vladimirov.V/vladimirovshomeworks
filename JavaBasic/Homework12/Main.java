package JavaBasic.Homework12;

import JavaBasic.Homework12.Controllers.MainController;

public class Main {
    public static void main(String[] args) {
        MainController mainController = new MainController();
        mainController.run();
    }
}