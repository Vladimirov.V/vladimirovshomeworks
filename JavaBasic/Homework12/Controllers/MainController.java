package JavaBasic.Homework12.Controllers;

import JavaBasic.Homework12.Models.Human;
import JavaBasic.Homework12.Models.Man;
import JavaBasic.Homework12.Models.Woman;
import JavaBasic.Homework12.Utils.FamilyOverflowException;
import JavaBasic.Homework12.Utils.Logger;

import java.util.HashMap;
import java.util.Scanner;

import static JavaBasic.Homework11.Utils.Colors.*;

public class MainController {
    FamilyController familyController = new FamilyController();
    Scanner scanner = new Scanner(System.in);
    boolean familiesLoaded;

    public void run() {

        int choice;

        while (true) {

            displayMenu();

            if (!scanner.hasNextInt()) {
                System.err.println(" Помилка: Ви ввели не число, будь ласка, введіть число. ");
                Logger.error("помилка вводу");
                scanner.nextLine();
                continue;
            }

            choice = scanner.nextInt();

            scanner.nextLine();

            switch (choice) {
                case 1 -> loadFamilies();
                case 2 -> showAllFamilies();
                case 3 -> showFamiliesBiggerThan();
                case 4 -> showFamiliesLessThan();
                case 5 -> showFamiliesWithMemberNumber();
                case 6 -> createNewFamily();
                case 7 -> deleteFamilyByIndex();
                case 8 -> editFamily();
                case 9 -> deleteAllChildrenOlderThen();
                case 0 -> {
                    System.out.println(YELLOW_BOLD + "До побачення!" + RESET);
                    return;
                }
                default -> {
                    System.err.println(" Помилка: Невідома команда, будь ласка, спробуйте ще раз. ");
                    Logger.error("помилка вводу");
                }
            }
        }
    }

    private void displayMenu() {
        System.out.println(GREEN_BOLD + """
                                               Меню
                               ______________________________________
                                 Оберіть дію:
                                    1. Завантажити дані
                                    2. Відобразити весь список сімей
                                    3. Відобразити список сімей, де кількість людей більша за задану
                                    4. Відобразити список сімей, де кількість людей менша за задану
                                    5. Підрахувати кількість сімей, де кількість членів дорівнює
                                    6. Створити нову родину
                                    7. Видалити сім'ю за індексом сім'ї у загальному списку
                                    8. Редагувати сім'ю за індексом сім'ї у загальному списку
                                    9. Видалити всіх дітей старше віку
                                    0. Вихід
                                ______________________________________
                """ + RESET);
    }

    public void displayEditFamilyMenu() {
        System.out.println(GREEN_BOLD +
                "Оберіть дію : \n" +
                "   1. Народити дитину\n" +
                "   2. Усиновити дитину\n" +
                "   0. Повернутися до головного меню\n"
                + RESET);
    }

    public void loadFamilies() {
        if (familiesLoaded){
            System.err.println("Ви вже завантажували сім'ї в цій сесії!");
            Logger.error("сім'ї вже завантажували в цій сесії");
            return;
        }

        familyController.loadFamilies();
        System.out.println(CYAN_BOLD + "Сім'ї завантажено!" + RESET);
        Logger.error("сім'ї завантажено");
        familiesLoaded = true;
    }

    public void showAllFamilies() {
        familyController.displayAllFamilies();
    }

    public void showFamiliesBiggerThan() {
        System.out.println(CYAN_BOLD + "Будь ласка , введіть кількість членів сім'ї (int) : " + RESET);

        if (!scanner.hasNextInt()) {
            System.err.println(" Помилка: Ви ввели не число, будь ласка, введіть число. ");
            Logger.error("помилка вводу");
            scanner.nextLine();
            return;
        }

        familyController.getFamiliesBiggerThan(scanner.nextInt());
    }

    public void showFamiliesLessThan() {
        System.out.println(CYAN_BOLD + "Будь ласка , введіть кількість членів сім'ї (int) : " + RESET);

        if (!scanner.hasNextInt()) {
            System.err.println(" Помилка: Ви ввели не число, будь ласка, введіть число. ");
            Logger.error("помилка вводу");
            scanner.nextLine();
            return;
        }

        familyController.getFamiliesLessThan(scanner.nextInt());

    }

    public void showFamiliesWithMemberNumber() {
        System.out.println(CYAN_BOLD + "Будь ласка , введіть кількість членів сім'ї (int) : " + RESET);

        if (!scanner.hasNextInt()) {
            System.err.println(" Помилка: Ви ввели не число, будь ласка, введіть число. ");
            Logger.error("помилка вводу");
            scanner.nextLine();
            return;
        }

        familyController.countFamiliesWithMemberNumber(scanner.nextInt());
    }

    public void createNewFamily() {

        try {
            System.out.println(CYAN + "Будь ласка , введіть ім'я матері : " + RESET);

            String motherName = scanner.nextLine();

            System.out.println(CYAN + "Будь ласка , введіть прізвище матері : " + RESET);

            String motherSurname = scanner.nextLine();

            System.out.println(CYAN + "Будь ласка , введіть дату народження народження матері (в форматі dd/MM/yyyy)  : " + RESET);

            String motherBirthDate = scanner.nextLine();

            System.out.println(CYAN + "Будь ласка , введіть iq матері : " + RESET);

            if (!scanner.hasNextInt()) {
                System.err.println(" Помилка: Ви ввели не число , нову сім'ю не створено! ");
                scanner.nextLine();
                return;
            }

            int motherIq = scanner.nextInt();

            scanner.nextLine();


            System.out.println(CYAN + "Будь ласка , введіть ім'я батька : " + RESET);

            String fatherName = scanner.nextLine();

            System.out.println(CYAN + "Будь ласка , введіть прізвище батька : " + RESET);

            String fatherSurname = scanner.nextLine();

            System.out.println(CYAN + "Будь ласка , введіть дату народження народження батька (в форматі dd/MM/yyyy)  : " + RESET);

            String fatherBirthDate = scanner.nextLine();

            System.out.println(CYAN + "Будь ласка , введіть iq батька : " + RESET);

            if (!scanner.hasNextInt()) {
                System.err.println(" Помилка: Ви ввели не число , нову сім'ю не створено! ");
                scanner.nextLine();
                return;
            }

            int fatherIq = scanner.nextInt();

            familyController.createNewFamily(new Woman(motherName, motherSurname, motherBirthDate, motherIq, null, new HashMap<>()),
                    new Man(fatherName, fatherSurname, fatherBirthDate, fatherIq, null, new HashMap<>()));
            System.out.println(CYAN + "Сім'ю успішно створено!" + RESET);
        } catch (RuntimeException runtimeException) {
            System.err.println(" Введені вами  данні не коректні , нову сім'ю не створено! ");
            Logger.error("помилка вводу");
        }

    }

    public void deleteFamilyByIndex() {
        if (familyController.getAllFamilies().isEmpty()) {
            System.err.println(" Помилка : не створено жодної сім'ї , дія неможлива! ");
            return;
        }

        System.out.println("Будь ласка , введіть порядковий номер сім'ї (ID) : ");

        if (!scanner.hasNextInt()) {
            System.err.println(" Помилка: Ви ввели не число , нову сім'ю не створено! ");
            Logger.error("помилка вводу");
            scanner.nextLine();
            return;
        }

        int id = scanner.nextInt() - 1;

        if (id < 0 || id > familyController.count()) {
            System.err.println("Помилка , за вашим ID нічого не знайдено !");
            return;
        }

        familyController.deleteFamilyByIndex(id);

        System.out.println(CYAN_BOLD + "Сім'ю успішно видалено!" + RESET);

    }

    public void editFamily() {

        if (familyController.getAllFamilies().isEmpty()) {
            System.err.println(" Помилка : не створено жодної сім'ї , дія неможлива! ");
            return;
        }

        while (true) {

            displayEditFamilyMenu();

            if (!scanner.hasNextInt()) {
                System.err.println(" Помилка: Ви ввели не число, будь ласка, введіть число. ");
                Logger.error("помилка вводу");
                scanner.nextLine();
                continue;
            }

            int editFamilyMenuChoice = scanner.nextInt();

            scanner.nextLine();

            switch (editFamilyMenuChoice) {
                case 1 -> bornChild();
                case 2 -> adoptChild();
                case 0 -> {
                    return;
                }
                default -> {
                    System.err.println(" Помилка: Невідома команда, будь ласка, спробуйте ще раз. ");
                    Logger.error("помилка вводу");
                }
            }
        }
    }

    private void bornChild() {
        System.out.println(CYAN + "Будь ласка, введіть порядковий номер сім'ї (ID) : " + RESET);

        if (!scanner.hasNextInt()) {
            System.err.println(" Помилка: Ви ввели не число, будь ласка, введіть число. ");
            Logger.error("помилка вводу");
            scanner.nextLine();
            return;
        }

        int id = scanner.nextInt() - 1;

        if (id < 0 || id > familyController.count()) {
            System.err.println("Помилка , за вашим ID нічого не знайдено !");
            return;
        }

        if (familyController.getFamilyById(id).getChildren().size() > 6) {
            Logger.error("Дітей в сім'ї забагато");
            throw new FamilyOverflowException("Дітей в сім'ї забагато!");

        }

        scanner.nextLine();

        System.out.println(CYAN + "Будь ласка , введіть ім'я дівчинки : " + RESET);

        String girlName = scanner.nextLine();

        System.out.println(CYAN + "Будь ласка , введіть ім'я хлопчика : " + RESET);

        String boyName = scanner.nextLine();

        familyController.bornChild(familyController.getFamilyById(id), girlName, boyName);

        System.out.println(CYAN + " Дитину народжено! " + RESET);

    }

    private void adoptChild() {
        System.out.println(CYAN + "Будь ласка, введіть порядковий номер сім'ї (ID) : " + RESET);

        if (!scanner.hasNextInt()) {
            System.err.println(" Помилка: Ви ввели не число, будь ласка, введіть число. ");
            Logger.error("помилка вводу");
            scanner.nextLine();
            return;
        }

        int id = scanner.nextInt() - 1;

        if (id < 0 || id > familyController.count()) {
            System.err.println("Помилка , за вашим ID нічого не знайдено !");
            return;
        }

        if (familyController.getFamilyById(id).getChildren().size() > 6) {
            Logger.error("Дітей в сім'ї забагато");
            throw new FamilyOverflowException("Дітей в сім'ї забагато!");
        }

        scanner.nextLine();

        System.out.println(CYAN + "Будь ласка , введіть ім'я дитини : " + RESET);

        String childName = scanner.nextLine();

        System.out.println(CYAN + "Будь ласка , введіть прізвище дитини : " + RESET);

        String childSurname = scanner.nextLine();

        System.out.println(CYAN + "Будь ласка , введіть дату народження народження дитини (в форматі dd/MM/yyyy)  : " + RESET);

        String childBirthDate = scanner.nextLine();

        System.out.println(CYAN + "Будь ласка , введіть iq дитини : " + RESET);

        if (!scanner.hasNextInt()) {
            System.err.println(" Помилка: Ви ввели не число , дитину не усиновлено! ");
            scanner.nextLine();
            return;
        }

        int childIq = scanner.nextInt();

        familyController.adoptChild(familyController.getFamilyById(id), new Human(childName, childSurname, childBirthDate, childIq, null, new HashMap<>()));

        System.out.println(CYAN + "Дитину успішно усиновлено!" + RESET);

    }

    public void deleteAllChildrenOlderThen() {
        if (familyController.getAllFamilies().isEmpty()) {
            System.err.println(" Помилка : не створено жодної сім'ї , дія неможлива! ");
            return;
        }

        System.out.println(CYAN_BOLD + "Будь ласка , введіть вік дітей (int) : " + RESET);

        if (!scanner.hasNextInt()) {
            System.err.println(" Помилка: Ви ввели не число, будь ласка, введіть число. ");
            Logger.error("помилка вводу");
            scanner.nextLine();
            return;
        }

        familyController.deleteAllChildrenOlderThen(scanner.nextInt());
    }
}