package JavaBasic.Homework12.Dao;

import JavaBasic.Homework12.Models.Family;
import JavaBasic.Homework12.Models.Human;
import JavaBasic.Homework12.Models.Pet;
import JavaBasic.Homework12.Utils.FileWorker;
import JavaBasic.Homework12.Utils.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CollectionFamilyDao implements FamilyDao {
    List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        Logger.info("отримання списку сімей");
        return families;
    }

    @Override
    public Family getFamilyByIndex(int idx) {
        if (families.size() > 0 && idx > -1 && idx < families.size()) {
            Logger.info("отримання сім'ї за номером");
            return families.get(idx);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int idx) {
        if (families.contains(families.get(idx))) {
            families.remove(idx);
            FileWorker.saveDataToFile(families);
            Logger.info("видалення сім'ї");
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (families.contains(family)) {
            families.remove(family);
            FileWorker.saveDataToFile(families);
            Logger.info("видалення сім'ї");
            return true;
        }
        return false;
    }

    @Override
    public void saveFamily(Family family) {

        boolean familyExists = false;

        for (int i = 0; i < families.size(); i++) {
            Family currentFamily = families.get(i);

            if (currentFamily.getFather().equals(family.getFather()) && currentFamily.getMother().equals(family.getMother())) {
                families.set(i, family);
                FileWorker.saveDataToFile(families);
                familyExists = true;
                break;
            }
        }

        if (!familyExists) {
            families.add(family);
            FileWorker.saveDataToFile(families);
        }
    }

    @Override
    public String getMotherSurname(int familyIdx) {
        Family family = getFamilyByIndex(familyIdx);
        if (family != null) {
            Human mother = family.getFather();
            if (mother != null) {
                return mother.getSurname();
            }
        }
        return null;
    }

    @Override
    public String getFatherSurname(int familyIdx) {
        Family family = getFamilyByIndex(familyIdx);
        if (family != null) {
            Human father = family.getFather();
            if (father != null) {
                return father.getSurname();
            }
        }
        return null;
    }

    @Override
    public boolean setChild(int familyIdx, Human child) {
        Family family = getFamilyByIndex(familyIdx);
        if (family != null) {
            family.addChild(child);
            FileWorker.saveDataToFile(families);
            Logger.info("всиновлення дитини");
            return true;
        }
        return false;
    }


    @Override
    public void deleteChild(Family family, Human child) {
        family.deleteChild(child);
        FileWorker.saveDataToFile(families);
        Logger.info("видалення дитини");
    }

    @Override
    public ArrayList<Human> getAllChildren(int familyIdx) {
        Family family = getFamilyByIndex(familyIdx);
        Logger.info("отримання списку дітей");
        if (family != null) {
            return family.getChildren();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public ArrayList<Human> getAllChildren() {
        ArrayList<Human> allChildren = new ArrayList<>();
        Logger.info("отримання списку дітей");
        for (Family family : families) {
            for (Human child : family.getChildren()) {
                allChildren.add(child);
            }
        }
        return allChildren;
    }

    @Override
    public int count() {
        return families.size();
    }

    @Override
    public Set<Pet> getPets(int idx) {
        Logger.info("отримання списку домашніх улюбленців");
        return getFamilyByIndex(idx).getPets();
    }

    @Override
    public boolean addPet(int familyIndex, Pet pet) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            family.addPet(pet);
            FileWorker.saveDataToFile(families);
            Logger.info("додавання домашнього улюбленця до сім'ї");
            return true;
        }
        return false;
    }

    @Override
    public void loadFamilies() {
        families.addAll( FileWorker.loadDataFromFile());
    }
}