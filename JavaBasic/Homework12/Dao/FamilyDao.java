package JavaBasic.Homework12.Dao;

import JavaBasic.Homework12.Models.Family;
import JavaBasic.Homework12.Models.Human;
import JavaBasic.Homework12.Models.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public interface FamilyDao {
    public List<Family> getAllFamilies();

    public Family getFamilyByIndex(int idx);

    public boolean deleteFamily(int idx);

    public boolean deleteFamily(Family family);

    public void saveFamily(Family family);

    public String getMotherSurname(int familyIdx);

    public String getFatherSurname(int familyIdx);

    public boolean setChild(int familyIdx, Human child);

    public void deleteChild(Family family, Human child);

    public ArrayList<Human> getAllChildren(int familyIdx);

    public int count();

    public Set<Pet> getPets(int idx);

    public ArrayList<Human> getAllChildren();

    public boolean addPet(int idx, Pet pet);

    public void loadFamilies();
}