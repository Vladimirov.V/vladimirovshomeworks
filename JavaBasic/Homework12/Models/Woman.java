package JavaBasic.Homework12.Models;

import JavaBasic.Homework12.Utils.DayOfWeek;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class Woman extends Human {

    public Woman(String name, String surname, String birthDate, int iq, Family family, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, family, schedule);
    }
    public void makeup() {
        System.out.println("Зараз підфарбуюся");
    }

    public String prettyFormat(){
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate birthLocalDate = Instant.ofEpochMilli(getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();

        return String.format("girl: {name='%s', surname='%s', birth date: '%s', iq='%d', schedule=%s", getName(), getSurname(), dateFormat.format(birthLocalDate), getIq(), getSchedule().toString());
    }

}