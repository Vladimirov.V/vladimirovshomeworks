package JavaBasic.Homework12.Models;

import JavaBasic.Homework12.Utils.FoulInterface;
import JavaBasic.Homework12.Utils.Species;

import java.io.Serializable;
import java.util.Set;

public class Dog extends Pet implements FoulInterface {

    private final Species species = Species.DOG;

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Привіт, хазяїн. Я собака - %s. Я скучив!\n", getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d,can fly=%b, number of legs=%d, has fur=%b habits=%s} ", species, getNickname(), getAge(), getTrickLevel(), species.getCanFly(), species.getNumberOfLegs(), species.getHasFur(), getHabits());
    }
}