package JavaBasic.Homework12.Service;

import JavaBasic.Homework12.Dao.CollectionFamilyDao;
import JavaBasic.Homework12.Dao.FamilyDao;
import JavaBasic.Homework12.Models.*;
import JavaBasic.Homework12.Utils.Colors;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FamilyService {
    FamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }


    public void displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();

        if (families.isEmpty()) {
            System.out.println(Colors.RED + "No families found." + Colors.RESET);
            return;
        }

        String familiesInfo = IntStream.range(0, families.size())
                .mapToObj(index -> (index + 1) + " : " + families.get(index).prettyFormat())
                .collect(Collectors.joining("\n"));

        System.out.println(Colors.GREEN + familiesInfo + Colors.RESET);
    }

    public ArrayList<Family> getFamiliesBiggerThan(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() > number)
                .collect(Collectors.toList()));
    }

    public ArrayList<Family> getFamiliesLessThan(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() < number)
                .collect(Collectors.toList()));
    }

    public ArrayList<Family> countFamiliesWithMemberNumber(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() == number)
                .collect(Collectors.toList()));
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
    }

    public void deleteFamilyByIndex(int idx) {
        familyDao.deleteFamily(idx);
    }

    public Family bornChild(Family family, String womenName, String manName) {
        boolean isBoy = Math.random() > 0.5;

        int iq = (family.getFather().getIq() + family.getMother().getIq()) / 2;

        if (isBoy) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String currentDateFormatted = LocalDate.now().format(formatter);

            Man child = new Man(manName, family.getFatherSurname(), currentDateFormatted, iq, null, new HashMap<>());
            adoptChild(family, child);
            familyDao.saveFamily(family);
            return family;
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String currentDateFormatted = LocalDate.now().format(formatter);

            Woman child = new Woman(womenName, family.getMotherSurname(), currentDateFormatted, iq, null, new HashMap<>());
            adoptChild(family, child);
            familyDao.saveFamily(family);
            return family;
        }
    }

    public Family adoptChild(Family family, Human child) {
        familyDao.setChild(familyDao.getAllFamilies().indexOf(family), child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().forEach(family -> {
            List<Human> childrenToRemove = family.getChildren().stream()
                    .filter(child -> {
                        LocalDate birthDate = Instant.ofEpochMilli(child.getBirthDate())
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate();

                        LocalDate ageLimitDate = birthDate.plusYears(age);

                        return LocalDate.now().isAfter(ageLimitDate);
                    })
                    .collect(Collectors.toList());

            childrenToRemove.forEach(family::deleteChild);
            familyDao.saveFamily(family);
        });
    }

    public ArrayList<Human> getAllChildren() {
        return familyDao.getAllChildren();
    }

    public int count() {
        return familyDao.count();
    }

    public Family getFamilyById(int idx) {
        return familyDao.getFamilyByIndex(idx);
    }

    public Set<Pet> getPets(int idx) {
        return familyDao.getPets(idx);
    }

    public void addPet(int idx, Pet pet) {
        familyDao.addPet(idx, pet);
    }

    public void loadFamilies() {
       familyDao.loadFamilies();
    }

}
