package JavaBasic.Homework10;

import java.util.Set;

public class DomesticCat extends Pet implements FoulInterface {

    private final Species species = Species.DOMESTICCAT;

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Привіт, хазяїн. Я кішка - %s. Я скучила!\n", getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Потрібно спихнути все на собаку...");
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d,can fly=%b, number of legs=%d, has fur=%b habits=%s} ", species, getNickname(), getAge(), getTrickLevel(), species.getCanFly(), species.getNumberOfLegs(), species.getHasFur(), getHabits());
    }
}