package JavaBasic.Homework10.Service;

import JavaBasic.Homework10.Colors;
import JavaBasic.Homework10.Dao.CollectionFamilyDao;
import JavaBasic.Homework10.Dao.FamilyDao;
import JavaBasic.Homework10.Family;
import JavaBasic.Homework10.Human;
import JavaBasic.Homework10.Pet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    FamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        String familiesInfo = familyDao.getAllFamilies().stream()
                .map(Family::toString)
                .collect(Collectors.joining("\n"));

        System.out.println(Colors.GREEN + familiesInfo + Colors.RESET);
    }

    public ArrayList<Family> getFamiliesBiggerThan(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() > number)
                .collect(Collectors.toList()));
    }

    public ArrayList<Family> getFamiliesLessThan(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() < number)
                .collect(Collectors.toList()));
    }

    public ArrayList<Family> countFamiliesWithMemberNumber(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() == number)
                .collect(Collectors.toList()));
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
    }

    public void deleteFamilyByIndex(int idx) {
        familyDao.deleteFamily(idx);
    }

    public Family bornChild(Family family, String womenName, String manName) {
        boolean isBoy = Math.random() > 0.5;

        if (isBoy) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String currentDateFormatted = LocalDate.now().format(formatter);

            Human child = new Human(manName, familyDao.getFatherSurname(familyDao.getAllFamilies().indexOf(family)), currentDateFormatted);
            adoptChild(family, child);
            familyDao.saveFamily(family);
            return family;
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String currentDateFormatted = LocalDate.now().format(formatter);

            Human child = new Human(womenName, familyDao.getMotherSurname(familyDao.getAllFamilies().indexOf(family)), currentDateFormatted);
            adoptChild(family, child);
            familyDao.saveFamily(family);
            return family;
        }
    }

    public Family adoptChild(Family family, Human child) {
        familyDao.setChild(familyDao.getAllFamilies().indexOf(family), child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().forEach(family -> {
            List<Human> childrenToRemove = family.getChildren().stream()
                    .filter(child -> {
                        LocalDate birthDate = Instant.ofEpochMilli(child.getBirthDate())
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate();

                        LocalDate ageLimitDate = birthDate.plusYears(age);

                        return LocalDate.now().isAfter(ageLimitDate);
                    })
                    .collect(Collectors.toList());

            childrenToRemove.forEach(family::deleteChild);
            familyDao.saveFamily(family);
        });
    }

    public ArrayList<Human> getAllChildren() {
        return familyDao.getAllChildren();
    }

    public int count() {
        return familyDao.count();
    }

    public Family getFamilyById(int idx) {
        return familyDao.getFamilyByIndex(idx);
    }

    public Set<Pet> getPets(int idx) {
        return familyDao.getPets(idx);
    }

    public void addPet(int idx, Pet pet) {
        familyDao.addPet(idx, pet);
    }

}
