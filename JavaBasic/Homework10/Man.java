package JavaBasic.Homework10;

import java.util.HashMap;
import java.util.Map;

public final class Man extends Human {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map<DayOfWeek, String> schedule;

    public Man(String name, String surname, String birthDate, int iq, Family family, HashMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = Validation.parseBirthDate(birthDate);
        this.iq = Validation.humanIq(iq);
        this.family = family;
        this.schedule = schedule;
    }

    public void repairCar() {
        System.out.println("Я пішов лагодити автівку");
    }
}