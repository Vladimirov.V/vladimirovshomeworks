package JavaBasic.Homework10;

public enum Species {
    DOMESTICCAT(false, 4, true),
    DOG(false, 4, true),
    FISH(false, 0, false),
    ROBOCAT(true, 4, false),
    UNKNOWN();

    private boolean canFly;
    private int numberOfLegs;
    private boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    Species() {
    }

    public boolean getCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean getHasFur() {
        return hasFur;
    }


}