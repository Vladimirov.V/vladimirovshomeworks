package JavaBasic.Homework11.Controller;

import JavaBasic.Homework11.Utils.Colors;
import JavaBasic.Homework11.Models.Family;
import JavaBasic.Homework11.Models.Human;
import JavaBasic.Homework11.Models.Pet;
import JavaBasic.Homework11.Service.FamilyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int number) {
        System.out.printf("\n %s Families bigger than %d %s: " +
                " %s \n", Colors.CYAN, number, Colors.RESET, familyService.getFamiliesBiggerThan(number));
    }

    public ArrayList<Family> getFamiliesLessThan(int number) {
        System.out.printf("\n%s Families less than %d %s: " +
                " %s \n", Colors.CYAN, number, Colors.RESET, familyService.getFamiliesLessThan(number));

        return familyService.getFamiliesLessThan(number);
    }

    public ArrayList<Family> countFamiliesWithMemberNumber(int number) {
        System.out.printf("\n%s Families with member number %d %s: " +
                " %s \n", Colors.CYAN, number, Colors.RESET, familyService.countFamiliesWithMemberNumber(number));
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother, father);
    }

    public void deleteFamilyByIndex(int idx) {
        familyService.deleteFamilyByIndex(idx);
    }

    public Family bornChild(Family family, String womenName, String manName) {
        return familyService.bornChild(family, womenName, manName);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int idx) {
        return familyService.getFamilyById(idx);
    }

    public Set<Pet> getPets(int idx) {
        return familyService.getPets(idx);
    }

    public void addPet(int idx, Pet pet) {
        familyService.addPet(idx, pet);
    }

    public void generateFamilies() {
        familyService.generateFamilies();
    }

}
