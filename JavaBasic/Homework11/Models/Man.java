package JavaBasic.Homework11.Models;

import JavaBasic.Homework11.Utils.DayOfWeek;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class Man extends Human {

    public Man(String name, String surname, String birthDate, int iq, Family family, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, family, schedule);
    }

    public void repairCar() {
        System.out.println("Я пішов лагодити автівку");
    }

    public String prettyFormat(){
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate birthLocalDate = Instant.ofEpochMilli(getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();

        return String.format("boy: {name='%s', surname='%s', birth date: '%s', iq='%d', schedule=%s", getName(), getSurname(), dateFormat.format(birthLocalDate), getIq(), getSchedule().toString());
    }

}