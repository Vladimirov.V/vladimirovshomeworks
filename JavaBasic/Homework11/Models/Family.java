package JavaBasic.Homework11.Models;

import JavaBasic.Homework11.Utils.Colors;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Family {

    private final Human mother;
    private final Human father;
    ArrayList<Human> children = new ArrayList<>();
    Set<Pet> pets = new HashSet<>();

    static {
        System.out.println(Colors.GREEN_BOLD_BRIGHT + "New Family class loaded" + Colors.RESET);
    }

    {
        System.out.println(Colors.CYAN_BOLD_BRIGHT + "Object created" + Colors.RESET);
    }

    public Family(Human mother, Human father) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.father = father;
    }

    public Human getMother() {
        return this.mother;
    }

    public String getMotherSurname() {
        return mother.getSurname();
    }

    public Human getFather() {
        return this.father;
    }

    public String getFatherSurname() {
        return father.getSurname();
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public Human getChild(Human child) {
        if (child == null || children.size() == 0) return null;
        int index = -1;
        for (int i = 0; i < children.size(); i++) {
            if (children.get(i).equals(child)) index = i;
        }
        if (index == -1) {
            return null;
        }
        return children.get(index);
    }


    public void setChildren(ArrayList<Human> children) {
        this.children.addAll(children);
    }

    public Set<Pet> getPets() {
        return this.pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        String petStr = pets != null ? pets.toString() : "none";

        String childrenStr = "none";
        if (children.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            for (Human child : children) {
                if (child == null) continue;
                stringBuilder.append(String.format("\n\t %s", child));
            }
            stringBuilder.append("\n]");
            childrenStr = new String(stringBuilder);
        }
        return String.format("\n Family : \n mother=%s,\n father=%s,\n children=%s,\n pets=%s\n \n", mother, father, childrenStr, petStr);
    }

    public String prettyFormat(){
        String petStr = pets != null ? pets.toString() : "[none]";

        String childrenStr = "[none]";
        if (children.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(" [ ");
            for (Human child : children) {
                if (child == null) continue;
                stringBuilder.append("\n     " + child.prettyFormat());
            }
            stringBuilder.append("\n]");
            childrenStr = new String(stringBuilder);
        }

        return String.format("\nFamily : \n mother: %s,\n father: %s,\n   children: %s \n       pets: %s \n \n", mother, father, childrenStr, petStr);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public void addChild(Human child) {
        if (children == null) {
            ArrayList<Human> childrenS = new ArrayList<>();
            childrenS.add(child);
            this.children = childrenS;
            return;
        }
        child.setFamily(this);
        children.add(child);
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index > children.size() - 1) return false;
        children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        if (children.size() == 0 || !children.contains(child)) return false;
        children.remove(child);
        return true;
    }

    private int petsHashCode() {
        if (pets == null) {
            return 0;
        }
        int hashCode = 0;
        for (Pet pet : pets) {
            hashCode = 31 * hashCode + Objects.hashCode(pet);
        }
        return hashCode;
    }

    private int childrenHashCode() {
        if (children == null) {
            return 0;
        }
        int hashCode = 0;
        for (Human child : children) {
            hashCode = 31 * hashCode + Objects.hashCode(child);
        }
        return hashCode;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father);
        result = result + childrenHashCode();
        result = result + petsHashCode();
        return result;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!Objects.equals(mother, family.mother)) return false;
        if (!Objects.equals(father, family.father)) return false;
        if (!Objects.equals(children, family.children)) return false;
        return Objects.equals(pets, family.pets);
    }

    public int countFamily() {

        int count = children != null ? children.size() : 0;
        if (father != null) count++;
        if (mother != null) count++;
        count = pets != null ? count + pets.size() : count + 0;
        return count;
    }
}