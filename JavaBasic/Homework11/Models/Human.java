package JavaBasic.Homework11.Models;

import JavaBasic.Homework11.Utils.Colors;
import JavaBasic.Homework11.Utils.DayOfWeek;
import JavaBasic.Homework11.Utils.Validation;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map<DayOfWeek, String> schedule;

    static {
        System.out.println(Colors.GREEN_BOLD_BRIGHT + "New Human class loaded" + Colors.RESET);
    }

    {
        System.out.println(Colors.CYAN_BOLD_BRIGHT + "Object created" + Colors.RESET);
    }

    public Human(String name, String surname, String birthDate, int iq, Family family, HashMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = Validation.parseBirthDate(birthDate);
        this.iq = Validation.humanIq(iq);
        this.family = family;
        this.schedule = schedule;
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = Validation.parseBirthDate(birthDate);
        this.iq = iq;
    }

    public Human(String name, String surname, String birthDate, int iq, Family family) {
        this.name = name;
        this.surname = surname;
        this.birthDate = Validation.parseBirthDate(birthDate);
        this.iq = Validation.humanIq(iq);
        this.family = family;
    }

    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = Validation.parseBirthDate(birthDate);
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = Validation.humanYear(birthDate);
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = Validation.humanIq(iq);
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate birthLocalDate = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        return String.format(" {name='%s', surname='%s', birth date: '%s', iq='%d', schedule=%s", name, surname, dateFormat.format(birthLocalDate), iq, schedule);
    }

    public String prettyFormat(){
        String isBoyOrGirl;

        if (getClass() == Man.class ){
            isBoyOrGirl = "boy";
        }else if (getClass() == Woman.class) {
            isBoyOrGirl = "girl";
        }else {
            isBoyOrGirl = "Human";
        }

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate birthLocalDate = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();

        return String.format(" %s: {name='%s', surname='%s', birth date: '%s', iq='%d', schedule=%s",isBoyOrGirl, name, surname, dateFormat.format(birthLocalDate), iq, schedule);

    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, schedule);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (birthDate != human.birthDate) return false;
        if (iq != human.iq) return false;
        if (!Objects.equals(name, human.name)) return false;
        if (!Objects.equals(surname, human.surname)) return false;
        return Objects.equals(schedule, human.schedule);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public boolean isTimeToFeed() {
        Random rnd = new Random();
        return rnd.nextBoolean();
    }

    public String describeAge() {
        LocalDate birthDatePrivate = LocalDate.ofInstant(Instant.ofEpochMilli(birthDate), ZoneId.systemDefault());
        LocalDate currentDate = LocalDate.now();

        Period period = Period.between(birthDatePrivate, currentDate);

        int years = period.getYears();
        int months = period.getMonths();
        int days = period.getDays();

        return String.format("%d years, %d months, %d days", years, months, days);
    }
}