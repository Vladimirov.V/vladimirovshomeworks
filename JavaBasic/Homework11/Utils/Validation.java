package JavaBasic.Homework11.Utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static JavaBasic.Homework5.Constants.*;

public class Validation {
    private static int isCorrectValue(int value, int minValue, int maxValue) {
        if (value <= minValue) return minValue;
        if (value >= maxValue) return maxValue;
        return value;
    }

    private static long isCorrectValue(long value, long minValue, long maxValue) {
        if (value <= minValue) return minValue;
        if (value >= maxValue) return maxValue;
        return value;
    }

    public static long parseBirthDate(String birthDate) {
        try {
            LocalDate date = LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            LocalDate minDate = LocalDate.of(1900, 1, 1);
            if (date.isBefore(minDate)) {
                date = LocalDate.of(1900, 1, 1);
            }
            if (date.isAfter(LocalDate.now())){
                date = LocalDate.now();
            }
            return date.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Invalid birth date format. Please use dd/MM/yyyy.", e);
        }
    }

    public static int petTrickLevel(int trickLevel) {
        return isCorrectValue(trickLevel, MIN_TRICK_LEVEL, MAX_TRICK_LEVEL);
    }

    public static int petAge(int age) {
        return isCorrectValue(age, MIN_PET_AGE, MAX_PET_AGE);

    }

    public static int humanIq(int iq) {
        return isCorrectValue(iq, MIN_HUMAN_IQ, MAX_HUMAN_IQ);
    }

    public static long humanYear(long year) {
        return isCorrectValue(year, MIN_HUMAN_YEAR, MAX_HUMAN_YEAR);
    }
}