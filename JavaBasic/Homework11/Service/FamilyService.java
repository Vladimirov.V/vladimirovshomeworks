package JavaBasic.Homework11.Service;

import JavaBasic.Homework11.Dao.CollectionFamilyDao;
import JavaBasic.Homework11.Dao.FamilyDao;
import JavaBasic.Homework11.Models.*;
import JavaBasic.Homework11.Utils.Colors;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FamilyService {
    FamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }


    public void displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();

        if (families.isEmpty()) {
            System.out.println(Colors.RED + "No families found." + Colors.RESET);
            return;
        }

        String familiesInfo = IntStream.range(0, families.size())
                .mapToObj(index -> (index + 1) + " : " + families.get(index).prettyFormat())
                .collect(Collectors.joining("\n"));

        System.out.println(Colors.GREEN + familiesInfo + Colors.RESET);
    }

    public ArrayList<Family> getFamiliesBiggerThan(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() > number)
                .collect(Collectors.toList()));
    }

    public ArrayList<Family> getFamiliesLessThan(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() < number)
                .collect(Collectors.toList()));
    }

    public ArrayList<Family> countFamiliesWithMemberNumber(int number) {
        return new ArrayList<>(getAllFamilies().stream()
                .filter(family -> family != null && family.countFamily() == number)
                .collect(Collectors.toList()));
    }

    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
    }

    public void deleteFamilyByIndex(int idx) {
        familyDao.deleteFamily(idx);
    }

    public Family bornChild(Family family, String womenName, String manName) {
        boolean isBoy = Math.random() > 0.5;

        int iq = (family.getFather().getIq() + family.getMother().getIq()) / 2;

        if (isBoy) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String currentDateFormatted = LocalDate.now().format(formatter);

            Man child = new Man(manName, family.getFatherSurname(), currentDateFormatted, iq, null, new HashMap<>());
            adoptChild(family, child);
            familyDao.saveFamily(family);
            return family;
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String currentDateFormatted = LocalDate.now().format(formatter);

            Woman child = new Woman(womenName, family.getMotherSurname(), currentDateFormatted, iq, null, new HashMap<>());
            adoptChild(family, child);
            familyDao.saveFamily(family);
            return family;
        }
    }

    public Family adoptChild(Family family, Human child) {
        familyDao.setChild(familyDao.getAllFamilies().indexOf(family), child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().forEach(family -> {
            List<Human> childrenToRemove = family.getChildren().stream()
                    .filter(child -> {
                        LocalDate birthDate = Instant.ofEpochMilli(child.getBirthDate())
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate();

                        LocalDate ageLimitDate = birthDate.plusYears(age);

                        return LocalDate.now().isAfter(ageLimitDate);
                    })
                    .collect(Collectors.toList());

            childrenToRemove.forEach(family::deleteChild);
            familyDao.saveFamily(family);
        });
    }

    public ArrayList<Human> getAllChildren() {
        return familyDao.getAllChildren();
    }

    public int count() {
        return familyDao.count();
    }

    public Family getFamilyById(int idx) {
        return familyDao.getFamilyByIndex(idx);
    }

    public Set<Pet> getPets(int idx) {
        return familyDao.getPets(idx);
    }

    public void addPet(int idx, Pet pet) {
        familyDao.addPet(idx, pet);
    }

    public void generateFamilies() {
        Woman human = new Woman("Ann", "Javav", "04/05/1976", 89, null, new HashMap<>());
        Man human1 = new Man("Jack", "Javav", "06/05/1975", 90, null, new HashMap<>());

        Woman human2 = new Woman("Alex", "Python", "05/03/1977", 80, null, new HashMap<>());
        Man human3 = new Man("Peter", "Python", "08/10/1978", 76, null, new HashMap<>());

        Woman human4 = new Woman("Jane", "Script", "23/05/1965", 94, null, new HashMap<>());
        Man human5 = new Man("Jakob", "Script", "22/11/1966", 89, null, new HashMap<>());

        Woman human6 = new Woman("Suzana", "C", "12/05/1961", 88, null, new HashMap<>());
        Man human7 = new Man("Ban", "C", "07/07/1963", 95, null, new HashMap<>());

        Woman child = new Woman("Mary", null, "26/04/2001", 78, null, new HashMap<>());
        Man child1 = new Man("Max", null, "06/07/2007", 78, null, new HashMap<>());
        Man child2 = new Man("Anton", null, "29/06/2003", 78, null, new HashMap<>());

        Pet pet = new Dog("Sharik", 4, 55, new HashSet<>());
        Pet pet1 = new Fish("Fish", 4, 55, new HashSet<>());
        Pet pet2 = new DomesticCat("Murka", 4, 55, new HashSet<>());

        Family family = new Family(human, human1);
        family.addChild(child);
        family.addPet(pet);

        Family family1 = new Family(human2, human3);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addPet(pet1);

        Family family2 = new Family(human4, human5);
        family2.addPet(pet2);

        Family family3 = new Family(human6, human7);

        familyDao.saveFamily(family);
        familyDao.saveFamily(family1);
        familyDao.saveFamily(family2);
        familyDao.saveFamily(family3);
    }

}
