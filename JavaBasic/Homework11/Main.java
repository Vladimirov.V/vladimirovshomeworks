package JavaBasic.Homework11;

import JavaBasic.Homework11.Controller.MainController;

public class Main {
    public static void main(String[] args) {
        MainController mainController = new MainController();
        mainController.run();
    }
}