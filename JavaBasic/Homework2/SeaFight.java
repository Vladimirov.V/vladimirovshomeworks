package JavaBasic.Homework2;

import java.util.Random;
import java.util.Scanner;

public class SeaFight {
    public static void main(String[] args) {

        String[][] map = {
                {"0", "1", "2", "3", "4", "5"}, {"1", "-", "-", "-", "-", "-"}, {"2", "-", "-", "-", "-", "-"},
                {"3", "-", "-", "-", "-", "-"}, {"4", "-", "-", "-", "-", "-"}, {"5", "-", "-", "-", "-", "-"}};

        Random rnd = new Random();

        int targetLine1 = rnd.nextInt(2, 5);
        int targetColumn1 = rnd.nextInt(2, 5);
        boolean random = rnd.nextBoolean();

        int targetLine;
        int targetColumn;
        int targetLine2;
        int targetColumn2;

        if (random) {
            targetLine = targetLine1;
            targetLine2 = targetLine1;
            targetColumn = targetColumn1 + 1;
            targetColumn2 = targetColumn1 - 1;
        } else {
            targetLine = targetLine1 + 1;
            targetLine2 = targetLine1 - 1;
            targetColumn = targetColumn1;
            targetColumn2 = targetColumn1;
        }

        boolean firstTarget = false;
        boolean secondTarget = false;
        boolean thirdTarget = false;

        System.out.println("All Set. Get ready to rumble!");

        Scanner scan = new Scanner(System.in);

        for (; ; ) {

            if (firstTarget && secondTarget && thirdTarget) {
                System.out.println();
                System.out.println("You have won!");
                System.out.println();

                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 6; j++) {
                        System.out.print(" " + map[i][j] + " " + "|");
                    }
                    System.out.println();
                }
                System.exit(0);
            }

            System.out.println();
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 6; j++) {
                    System.out.print(" " + map[i][j] + " " + "|");
                }
                System.out.println();
            }

            System.out.println();
            System.out.println("Please , enter the line (1 - 5)");

            if (scan.hasNextInt()) {

                int line = scan.nextInt();

                if (line > 0 && line < 6) {

                    System.out.println("Please , enter the column (1 - 5)");

                    if (scan.hasNextInt()) {

                        int column = scan.nextInt();

                        if (column > 0 && column < 6) {

                            if (line == targetLine && column == targetColumn) {

                                map[line][column] = "x";

                                firstTarget = true;

                            } else if (line == targetLine1 && column == targetColumn1) {
                                map[line][column] = "x";
                                secondTarget = true;

                            } else if (line == targetLine2 && column == targetColumn2) {
                                map[line][column] = "x";
                                thirdTarget = true;

                            } else {
                                System.out.printf("You shot at : ( %s , %s )", line, column);
                                map[line][column] = "*";
                                scan.nextLine();
                            }
                        } else {
                            System.err.println("Your number not on map! Please , try again");
                            scan.nextLine();
                        }
                    } else {
                        System.err.println("You entered not a number , please enter a number");
                        scan.nextLine();
                    }
                } else {
                    System.err.println("Your number not on map! Please , try again");
                    scan.nextLine();
                }
            } else {
                System.err.println("You entered not a number , please enter a number");
                scan.nextLine();
            }
        }
    }
}