package JavaBasic.Homework6.Tests;

import JavaBasic.Homework6.Dog;
import JavaBasic.Homework6.Family;
import JavaBasic.Homework6.Human;
import JavaBasic.Homework6.Pet;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTests {
    @Test
    public void testToString() {
        Human parent1 = new Human("Angelina", "Java", 1986);

        Human parent2 = new Human("Vasily", "Java", 1988);

        Human child = new Human("Dan", "Java", 2012);

        Pet dog = new Dog("Roy", 5, 66, new String[]{});

        Family family = new Family(parent1, parent2);

        family.setPet(dog);
        family.addChild(child);

        String result = family.toString();

        String trueResult = "\n Family : \n" +
                " mother=Human{name='Angelina', surname='Java', year='1986', iq='0', schedule=[null],\n" +
                " father=Human{name='Vasily', surname='Java', year='1988', iq='0', schedule=[null],\n" +
                " children=[\n" +
                "\t Human{name='Dan', surname='Java', year='2012', iq='0', schedule=[null]\n" +
                "],\n" +
                " pet=DOG{nickname='Roy', age=5, trickLevel=66,can fly=false, number of legs=4, has fur=true habits=[]} \n" +
                " \n";

        assertEquals(trueResult, result);
    }

    @Test
    public void testDeleteChild() {
        Human parentMock = Mockito.mock(Human.class);
        Mockito.when(parentMock.toString()).thenReturn("Parent1");

        Human parent2Mock = Mockito.mock(Human.class);
        Mockito.when(parent2Mock.toString()).thenReturn("Parent2");

        Human child1Mock = Mockito.mock(Human.class);
        Mockito.when(child1Mock.toString()).thenReturn("Child1");

        Human child2Mock = Mockito.mock(Human.class);
        Mockito.when(child2Mock.toString()).thenReturn("Child2");

        Human child3Mock = Mockito.mock(Human.class);
        Mockito.when(child3Mock.toString()).thenReturn("Child3");

        Family family = new Family(parentMock, parent2Mock);
        family.addChild(child1Mock);
        family.addChild(child2Mock);
        family.addChild(child3Mock);

        family.deleteChild(child1Mock);

        String result = family.toString();

        String trueResult = "\n" +
                " Family : \n" +
                " mother=Parent1,\n" +
                " father=Parent2,\n" +
                " children=[\n" +
                "\t Child2\n" +
                "\t Child3\n" +
                "],\n" +
                " pet=none\n" +
                " \n";

        assertEquals(trueResult, result);
    }

    @Test
    public void testNotDeleteChild() {
        Human parentMock = Mockito.mock(Human.class);
        Mockito.when(parentMock.toString()).thenReturn("Parent1");

        Human parent2Mock = Mockito.mock(Human.class);
        Mockito.when(parent2Mock.toString()).thenReturn("Parent2");

        Human child1Mock = Mockito.mock(Human.class);
        Mockito.when(child1Mock.toString()).thenReturn("Child1");

        Human child2Mock = Mockito.mock(Human.class);
        Mockito.when(child2Mock.toString()).thenReturn("Child2");

        Human child3Mock = Mockito.mock(Human.class);
        Mockito.when(child3Mock.toString()).thenReturn("Child3");

        Human anotherChildMock = Mockito.mock(Human.class);
        Mockito.when(anotherChildMock.toString()).thenReturn("Another child");

        Family family = new Family(parentMock, parent2Mock);
        family.addChild(child1Mock);
        family.addChild(child2Mock);
        family.addChild(child3Mock);

        family.deleteChild(anotherChildMock);

        String result = family.toString();

        String trueResult = "\n" +
                " Family : \n" +
                " mother=Parent1,\n" +
                " father=Parent2,\n" +
                " children=[\n" +
                "\t Child1\n" +
                "\t Child2\n" +
                "\t Child3\n" +
                "],\n" +
                " pet=none\n" +
                " \n";

        assertEquals(trueResult, result);
    }

    @Test
    public void testDeleteChildWithIndex() {
        Human parentMock = Mockito.mock(Human.class);
        Mockito.when(parentMock.toString()).thenReturn("Parent1");

        Human parent2Mock = Mockito.mock(Human.class);
        Mockito.when(parent2Mock.toString()).thenReturn("Parent2");

        Human child1Mock = Mockito.mock(Human.class);
        Mockito.when(child1Mock.toString()).thenReturn("Child1");

        Human child2Mock = Mockito.mock(Human.class);
        Mockito.when(child2Mock.toString()).thenReturn("Child2");

        Human child3Mock = Mockito.mock(Human.class);
        Mockito.when(child3Mock.toString()).thenReturn("Child3");

        Family family = new Family(parentMock, parent2Mock);
        family.addChild(child1Mock);
        family.addChild(child2Mock);
        family.addChild(child3Mock);

        family.deleteChild(0);

        String result = family.toString();

        String trueResult = "\n" +
                " Family : \n" +
                " mother=Parent1,\n" +
                " father=Parent2,\n" +
                " children=[\n" +
                "\t Child2\n" +
                "\t Child3\n" +
                "],\n" +
                " pet=none\n" +
                " \n";

        assertEquals(trueResult, result);

    }

    @Test
    public void testNotDeleteChildWithIndex() {
        Human parentMock = Mockito.mock(Human.class);
        Mockito.when(parentMock.toString()).thenReturn("Parent1");

        Human parent2Mock = Mockito.mock(Human.class);
        Mockito.when(parent2Mock.toString()).thenReturn("Parent2");

        Human child1Mock = Mockito.mock(Human.class);
        Mockito.when(child1Mock.toString()).thenReturn("Child1");

        Human child2Mock = Mockito.mock(Human.class);
        Mockito.when(child2Mock.toString()).thenReturn("Child2");

        Human child3Mock = Mockito.mock(Human.class);
        Mockito.when(child3Mock.toString()).thenReturn("Child3");

        Family family = new Family(parentMock, parent2Mock);
        family.addChild(child1Mock);
        family.addChild(child2Mock);
        family.addChild(child3Mock);

        family.deleteChild(3);

        String result = family.toString();

        String trueResult = "\n" +
                " Family : \n" +
                " mother=Parent1,\n" +
                " father=Parent2,\n" +
                " children=[\n" +
                "\t Child1\n" +
                "\t Child2\n" +
                "\t Child3\n" +
                "],\n" +
                " pet=none\n" +
                " \n";

        assertEquals(trueResult, result);

    }

    @Test
    public void testAddChild() {
        Human parentMock = Mockito.mock(Human.class);
        Mockito.when(parentMock.toString()).thenReturn("Parent1");

        Human parent2Mock = Mockito.mock(Human.class);
        Mockito.when(parent2Mock.toString()).thenReturn("Parent2");

        Human child1Mock = Mockito.mock(Human.class);
        Mockito.when(child1Mock.toString()).thenReturn("Child1");

        Human child2Mock = Mockito.mock(Human.class);
        Mockito.when(child2Mock.toString()).thenReturn("Child2");

        Human child3Mock = Mockito.mock(Human.class);
        Mockito.when(child3Mock.toString()).thenReturn("Child3");

        Family family = new Family(parentMock, parent2Mock);
        family.addChild(child1Mock);
        family.addChild(child2Mock);

        int langBeforeAdd = family.getChildren().length;

        family.addChild(child3Mock);

        int langAfterAdd = family.getChildren().length;

        assertEquals(langBeforeAdd, langAfterAdd - 1);
        assertEquals(family.getChild(child3Mock), child3Mock);
    }

    @Test
    public void testCountFamily() {
        Human parentMock = Mockito.mock(Human.class);
        Mockito.when(parentMock.toString()).thenReturn("Parent1");

        Human parent2Mock = Mockito.mock(Human.class);
        Mockito.when(parent2Mock.toString()).thenReturn("Parent2");

        Human child1Mock = Mockito.mock(Human.class);
        Mockito.when(child1Mock.toString()).thenReturn("Child1");

        Human child2Mock = Mockito.mock(Human.class);
        Mockito.when(child2Mock.toString()).thenReturn("Child2");

        Human child3Mock = Mockito.mock(Human.class);
        Mockito.when(child3Mock.toString()).thenReturn("Child3");

        Pet petMock = Mockito.mock(Pet.class);
        Mockito.when(petMock.toString()).thenReturn("Pet");

        Family family = new Family(parentMock, parent2Mock);

        family.addChild(child1Mock);
        family.addChild(child2Mock);
        family.addChild(child3Mock);

        family.setPet(petMock);

        assertEquals(family.countFamily(), 6);
    }
}