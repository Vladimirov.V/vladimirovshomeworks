package JavaBasic.Homework6.Tests;

import JavaBasic.Homework6.DayOfWeek;
import JavaBasic.Homework6.Family;
import JavaBasic.Homework6.Human;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class HumanTests {
    @Test
    public void testToString() {
        Family familyMock = Mockito.mock(Family.class);
        Human testHuman = new Human("Vasily", "Java", 1988, 97, familyMock, new String[][]{{DayOfWeek.MONDAY.name(), "go to the cinema"}, {DayOfWeek.TUESDAY.name(), "learn java"}, {DayOfWeek.WEDNESDAY.name(), "chilling"}});
        String result = testHuman.toString();
        String trueResult = "Human{name='Vasily', surname='Java', year='1988', iq='97', schedule=[[MONDAY, go to the cinema], [TUESDAY, learn java], [WEDNESDAY, chilling]]";
        assertEquals(trueResult, result);
    }

    @Test
    public void testNotFullToString() {
        Family familyMock = Mockito.mock(Family.class);
        Human testHuman = new Human("Vasily", "Java", 1988, 97, familyMock);
        String result = testHuman.toString();
        String trueResult = "Human{name='Vasily', surname='Java', year='1988', iq='97', schedule=null";
        assertEquals(trueResult, result);
    }

    @Test
    public void testVeryNotFullToString() {
        Human testHuman = new Human("Vasily", "Java", 1988);
        String result = testHuman.toString();
        String trueResult = "Human{name='Vasily', surname='Java', year='1988', iq='0', schedule=[null]";
        assertEquals(trueResult, result);
    }

    @Test
    public void testNullToString() {
        Human testHuman = new Human();
        String result = testHuman.toString();
        String trueResult = "Human{name='null', surname='null', year='0', iq='0', schedule=null";
        assertEquals(trueResult, result);
    }

    @Test
    public void testFullHumanEquals() {
        Family mockFamily = new Mockito().mock(Family.class);

        Human testHuman1 = new Human("Ivan", "Ivanovich", 1986, 75, mockFamily, new String[][]{});
        Human testHuman2 = new Human("Ivan", "Ivanovich", 1986, 75, mockFamily, new String[][]{});
        assertTrue(testHuman1.equals(testHuman2));
    }

    @Test
    public void testFullHumanNotEquals() {
        Family mockFamily = new Mockito().mock(Family.class);

        Human testHuman1 = new Human("Ivan", "Ivanovich", 1986, 75, mockFamily, new String[][]{});
        Human testHuman2 = new Human("Ivana", "Ivanovish", 1988, 77, mockFamily, new String[][]{});

        assertFalse(testHuman1.equals(testHuman2));
    }

    @Test
    public void testNotFullHumanEquals() {
        Human testHuman1 = new Human("Ivan", "Ivanovich", 1986);
        Human testHuman2 = new Human("Ivan", "Ivanovich", 1986);
//TODO : Problem with null
        //    assertTrue(testHuman1.equals(testHuman2));
        System.err.println("TODO : problem with null");
    }

    @Test
    public void testNotFullHumanNotEquals() {
        Human testHuman1 = new Human("Ivan", "Ivanovich", 1986);
        Human testHuman2 = new Human("Ivana", "Ivanovish", 1984);

        assertFalse(testHuman1.equals(testHuman2));
    }

    @Test
    public void testNullHumanEquals() {
        Human testHuman1 = new Human();
        Human testHuman2 = new Human();
//TODO : Problem with null
        //    assertTrue(testHuman1.equals(testHuman2));
        System.err.println("TODO : problem with null");
    }

    @Test
    public void testNullHumanNotEquals() {
        Human testHuman1 = new Human();
        Human testHuman2 = new Human("Ivan", "Ivanovich", 1986);

        assertFalse(testHuman1.equals(testHuman2));
    }

    @Test
    public void testHumanHashcode() {
//TODO : Problem with null
        Family mockFamily = new Mockito().mock(Family.class);

        Human testHuman1 = new Human("Ivan", "Ivanovich", 1986,55,mockFamily,new String[][]{});
        Human testHuman2 = new Human("Ivan", "Ivanovich", 1986,55,mockFamily,new String[][]{});
        int result = testHuman1.hashCode();
        int secondResult = testHuman2.hashCode();
        assertEquals(result, secondResult);
        System.err.println("TODO : problem with null");
    }
}