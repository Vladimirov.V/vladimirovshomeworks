package JavaBasic.Homework6.Tests;

import JavaBasic.Homework6.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PetTests {
    @Test
    public void testToString() {
        Pet testPet = new Dog("Roy", 5, 78, new String[]{"eat", "sleep", "walking"});
        String result = testPet.toString();
        String trueResult = "DOG{nickname='Roy', age=5, trickLevel=78,can fly=false, number of legs=4, has fur=true habits=[eat, sleep, walking]} ";
        assertEquals(trueResult, result);
    }

    @Test
    public void testNotFullToString() {
        Pet testPet = new Dog("Roy", 4, 55, new String[]{});
        String result = testPet.toString();
        String trueResult = "DOG{nickname='Roy', age=4, trickLevel=55,can fly=false, number of legs=4, has fur=true habits=[]} ";
        assertEquals(trueResult, result);

    }

    @Test
    public void testNullPetToString() {
        Pet testPet = new Cow();
        String result = testPet.toString();
        String trueResult = "UNKNOWN{nickname='null', age=0, trickLevel=0,can fly=false, number of legs=0, has fur=false habits=null} ";
        assertEquals(trueResult, result);
    }

    @Test
    public void testFullPetEquals() {
        Pet testPet1 = new Dog( "Rex", 5, 55, new String[]{});
        Pet testPet2 = new Dog( "Rex", 5, 55, new String[]{});

        assertTrue(testPet1.equals(testPet2));
    }

    @Test
    public void testFullPetNotEquals() {
        Pet testPet1 = new DomesticCat( "Murchik", 5, 55, new String[]{});
        Pet testPet2 = new RoboCat( "Murchik3000", 4, 35, new String[]{});

        assertFalse(testPet1.equals(testPet2));
    }

    @Test
    public void testNotFullPetEquals() {
        Pet testPet1 = new Fish( null,0,0,null);
        Pet testPet2 = new Fish( null,0,0,null);

        assertTrue(testPet1.equals(testPet2));
    }

    @Test
    public void testNotFullPetNotEquals() {
        Pet testPet1 = new Dog(null,0,0,null);
        Pet testPet2 = new DomesticCat(null,1,0,null);

        assertFalse(testPet1.equals(testPet2));
    }

    @Test
    public void testNullPetEquals() {
        Pet testPet1 = new Cow();
        Pet testPet2 = new Cow();

        assertTrue(testPet1.equals(testPet2));
    }

    @Test
    public void testNullPetNotEquals() {
        Pet testPet1 = new Cow();
        Pet testPet2 = new Dog(null,0,0,null);

        assertFalse(testPet1.equals(testPet2));
    }

    @Test
    public void testPetHashcode() {
        Pet testPet1 = new Fish("Gold", 5, 55, new String[]{});
        Pet testPet2 = new Fish("Gold", 5, 55, new String[]{});
        int result = testPet1.hashCode();
        int secondResult = testPet2.hashCode();
        assertEquals(result, secondResult);
    }
}