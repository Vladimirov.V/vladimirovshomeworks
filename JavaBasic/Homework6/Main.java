package JavaBasic.Homework6;

public class Main {
    public static void main(String[] args) {

        Dog roy = new Dog("Roy", 6, 77, new String[]{});

        System.out.println(roy);

        DomesticCat cat = new DomesticCat("Murchik", 3, 99, new String[]{});

        System.out.println(cat);

        Fish fish = new Fish("Gold", 1, 1, new String[]{});

        System.out.println(fish);

        RoboCat roboCat = new RoboCat("Cat3000", 4, 0, new String[]{});

        System.out.println(roboCat);

        Pet cow = new Cow();
        System.out.println(cow);

    }
}