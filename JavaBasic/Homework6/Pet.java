package JavaBasic.Homework6;

import java.util.Arrays;

public abstract class Pet {
    protected Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("New Pet class loaded");
    }

    {
        System.out.println("Object created");
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = Validation.petAge(age);
        this.trickLevel = Validation.petTrickLevel(trickLevel);
        this.habits = habits;
    }

    public Pet(String nickname) {
        this(nickname, 0, 0, new String[]{});
    }

    public Pet() {
    }

    public Species getSpices() {
        return species;
    }

    public void setSpices(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = Validation.petAge(age);
    }

    public int getTrickLevel() {

        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = Validation.petTrickLevel(trickLevel);
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public abstract String toString();

    @Override
    public int hashCode() {
        int result = species.hashCode();
        result = 31 * result + nickname.hashCode();
        result = 31 * result + age;
        result = 31 * result + trickLevel;
        result = 31 * result + Arrays.deepHashCode(habits);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (this.nickname == null && ((Pet) o).getNickname() == null) {

            Pet pet = (Pet) o;

            if (age != pet.getAge()) return false;
            if (trickLevel != pet.getTrickLevel()) return false;
            if (!species.equals(pet.getSpices())) return false;
            return Arrays.deepEquals(habits, pet.getHabits());
        }

        Pet pet = (Pet) o;

        if (age != pet.age) return false;
        if (trickLevel != pet.trickLevel) return false;
        if (!species.equals(pet.species)) return false;
        if (!nickname.equals(pet.nickname)) return false;
        return Arrays.deepEquals(habits, pet.habits);
    }

    @Override
    protected void finalize() {
        System.out.println(toString());
    }

    public void eat() {
        System.out.printf("\n %s : Я ї'м!\n", getNickname());
    }

    public abstract void respond();


}