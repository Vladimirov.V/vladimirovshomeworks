package JavaBasic.Homework6;

public final class Woman extends Human {
    @Override
    public void greetPet() {
        if (getFamily() == null || getFamily().getPet() == null) return;
        System.out.printf("Привіт, %s, ти скучив? " + getFamily().getPet().getNickname());
    }

    public void makeup() {
        System.out.println("Зараз підфарбуюся");
    }
}