package JavaBasic.Homework6;

import java.util.Arrays;

public class RoboCat extends Pet {

    private final Species species = Species.ROBOCAT;

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Привіт, хазяїн. Я робот-кішка - %s. Я скучив!\n", getNickname());
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d,can fly=%b, number of legs=%d, has fur=%b habits=%s} ", species, getNickname(), getAge(), getTrickLevel(), species.getCanFly(), species.getNumberOfLegs(), species.getHasFur(), Arrays.toString(getHabits()));
    }
}