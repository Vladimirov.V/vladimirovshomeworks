package JavaBasic.Homework3;

import java.util.Scanner;

public class Diary {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];

        schedule[0][0] = "monday";
        schedule[0][1] = "Вчити Java. Вигуляти песика";

        schedule[1][0] = "tuesday";
        schedule[1][1] = "Вчити Java. Сходити в кінотеатр";

        schedule[2][0] = "Wednesday";
        schedule[2][1] = "Вчити Java. Вчити квантову фізику";

        schedule[3][0] = "Thursday";
        schedule[3][1] = "Вчити Java. Покататися на велосипеді";

        schedule[4][0] = "Friday";
        schedule[4][1] = "Вчити Java. Вчити алгебру";

        schedule[5][0] = "Saturday";
        schedule[5][1] = "Вчити Java. Грати у відеоігри";

        schedule[6][0] = "Sunday";
        schedule[6][1] = "Вчити Java. Зловити депресію , бо завтра понеділок";

        Scanner scanner = new Scanner(System.in);

        for (; ; ) {
            System.out.println();
            System.out.println();
            System.out.println("Please, input the day of the week or [helper]: ");

            String line = scanner.nextLine();

            switch (line.toLowerCase().trim()) {

                case ("monday"):
                    System.out.printf("Your tasks for Monday: %s", schedule[0][1]);
                    break;
                case ("change monday"):
                    System.out.println("Please, input new tasks for Monday.");
                    String changingMondayTasks = scanner.nextLine();
                    schedule[0][1] = changingMondayTasks;
                    System.out.println("The new tasks are saved!");
                    break;

                case ("tuesday"):
                    System.out.printf("Your tasks for Tuesday: %s", schedule[1][1]);
                    break;
                case ("change tuesday"):
                    System.out.println("Please, input new tasks for Tuesday.");
                    String changingTuesdayTasks = scanner.nextLine();
                    schedule[1][1] = changingTuesdayTasks;
                    System.out.println("The new tasks are saved!");
                    break;

                case ("wednesday"):
                    System.out.printf("Your tasks for Wednesday: %s", schedule[2][1]);
                    break;
                case ("change wednesday"):
                    System.out.println("Please, input new tasks for Wednesday.");
                    String changingWednesdayTasks = scanner.nextLine();
                    schedule[2][1] = changingWednesdayTasks;
                    System.out.println("The new tasks are saved!");
                    break;

                case ("thursday"):
                    System.out.printf("Your tasks for Thursday: %s", schedule[3][1]);
                    break;
                case ("change thursday"):
                    System.out.println("Please, input new tasks for Thursday.");
                    String changingThursdayTasks = scanner.nextLine();
                    schedule[3][1] = changingThursdayTasks;
                    System.out.println("The new tasks are saved!");
                    break;

                case ("friday"):
                    System.out.printf("Your tasks for Friday: %s", schedule[4][1]);
                    break;
                case ("change friday"):
                    System.out.println("Please, input new tasks for Friday.");
                    String changingFridayTasks = scanner.nextLine();
                    schedule[4][1] = changingFridayTasks;
                    System.out.println("The new tasks are saved!");
                    break;

                case ("saturday"):
                    System.out.printf("Your tasks for Saturday: %s", schedule[5][1]);
                    break;
                case ("change saturday"):
                    System.out.println("Please, input new tasks for Saturday.");
                    String changingSaturdayTasks = scanner.nextLine();
                    schedule[5][1] = changingSaturdayTasks;
                    System.out.println("The new tasks are saved!");
                    break;

                case ("sunday"):
                    System.out.printf("Your tasks for Sunday: %s", schedule[6][1]);
                    break;
                case ("change sunday"):
                    System.out.println("Please, input new tasks for Sunday.");
                    String changingSundayTasks = scanner.nextLine();
                    schedule[6][1] = changingSundayTasks;
                    System.out.println("The new tasks are saved!");
                    break;

                case ("helper"):
                    System.out.println(" change + пробіл + day of the week - задати нові завдання на обраний день ");
                    System.out.println(" exit - завершення роботи програми");
                    break;

                case ("exit"):
                    System.out.println("Bye");
                    System.exit(0);

                default:
                    System.err.println("Sorry, I don't understand you, please try again.");
                    break;
            }
        }
    }
}